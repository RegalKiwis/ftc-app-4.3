package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.HardwareMap;

/**
 * This interface provides a method for initializing hardware that should be shared between hardware devices.
 */
public interface Initializable {
    public void initHardware(HardwareMap hwMap);
}
