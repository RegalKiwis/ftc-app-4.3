package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.teamcode.DriveTrain.DriveTrain;

/**
 * Class containing a DcMotor used for a linear slide mechanism along with
 * methods for moving a particular distance based on the spool circumference
 * and the length of string being spooled together.
 */
public class LinearSlideMotor implements Hardware {
    private DcMotor[] linearSlideMotors = new DcMotor[1];
    private Telemetry telemetry;
    private final MotorInformation motorInfo = new RevHDHex_40Gear();

    boolean raised = false;

    //72 tooth gear to 125 gear
    private double gearRatio = 40 / 10.0;
    private double sprocketDiameter = 1.409449; //Inches, 15 tooth sprockets
    private double sprocketCircumference = sprocketDiameter * Math.PI;
    private double encodersPerInch = motorInfo.encoderCount() * gearRatio / sprocketCircumference;

    private double movementLength = 6.5;//Inches
    private int encodersMoveSlide = (int) (movementLength * encodersPerInch);
    private final double power = .9;

    @Override
    public void initHardware(HardwareMap hwMap, Telemetry telemetry) {
        for (int i = 0; i < linearSlideMotors.length; i++)
        linearSlideMotors[i] = hwMap.dcMotor.get("linearSlideMotors" + i);
        this.telemetry = telemetry;
        DcMotorControl.setZeroBehave(DcMotor.ZeroPowerBehavior.BRAKE, linearSlideMotors); //To hold in the air

        stop();

        DcMotorControl.setDirection(DcMotorSimple.Direction.REVERSE, linearSlideMotors);
    }

    @Override
    public void telemetryData() {
       telemetry.addData("Is Raised", raised);
       telemetry.addData("Current Encoder Position", linearSlideMotors[0].getCurrentPosition());
       telemetry.update();
    }

    public void stop() {
        DcMotorControl.setPower(0, linearSlideMotors);
        DcMotorControl.setMode(DcMotor.RunMode.RUN_USING_ENCODER, linearSlideMotors);
    }

    /**
     * The power of the linear slide motors.  A positive power will rotate the chain such that the slide will move upward.
     * @param power Power of the linear slide motors
     */
    private void setPower(double power) {
        DcMotorControl.setPower(power, linearSlideMotors);
    }

    private void setPower() {
        DcMotorControl.setPower(power, linearSlideMotors);
    }

    public void raise() {
       DcMotorControl.setTargetPositionRelative(encodersMoveSlide, linearSlideMotors);
       DcMotorControl.setMode(DcMotor.RunMode.RUN_TO_POSITION, linearSlideMotors);

       setPower();
       while (DcMotorControl.anyMotorBusy(linearSlideMotors)) {
            telemetryData();
       }
       stop();
    }

    public void lower() {
        DcMotorControl.setTargetPositionRelative(-encodersMoveSlide, linearSlideMotors);
        DcMotorControl.setMode(DcMotor.RunMode.RUN_TO_POSITION, linearSlideMotors);

        setPower();
        while (DcMotorControl.anyMotorBusy(linearSlideMotors)) {
            telemetryData();
        }
        stop();
    }

    public void togglePosition(boolean buttonPress) {
        if (!linearSlideMotors[0].isBusy()) {
            if (buttonPress) {

                if (!raised) {
                    DcMotorControl.setTargetPositionRelative(encodersMoveSlide, linearSlideMotors);
                } else {
                    DcMotorControl.setTargetPositionRelative(-encodersMoveSlide, linearSlideMotors);
                }
                DcMotorControl.setMode(DcMotor.RunMode.RUN_TO_POSITION, linearSlideMotors);

                setPower();
                raised = !raised;
            } else {
                stop();
            }
        }
    }
}
