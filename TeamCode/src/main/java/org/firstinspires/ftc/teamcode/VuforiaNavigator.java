package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.HardwareMap;

import org.firstinspires.ftc.robotcontroller.external.samples.ConceptVuforiaNavigation;
import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.robotcore.external.navigation.RelicRecoveryVuMark;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackable;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackables;

/**
 * Class containing methods for driving the robot using the image targets to navigate.
 * @see ConceptVuforiaNavigation
 */
public class VuforiaNavigator implements Hardware {
    private VuforiaLocalizer vuforia;
    private Telemetry telemetry;

    //Trackable image targets
    private VuforiaTrackables relicVuMark;
    private VuforiaTrackable relicTemplate;

    /**
     * @return VuMark enumeration associated with
     * the currently visible image target.
     */
    public RelicRecoveryVuMark visibleImage() {
        return RelicRecoveryVuMark.from(relicTemplate);
    }

    @Override
    public void telemetryData() {
        telemetry.addData("VuMark?", visibleImage());
        telemetry.update();
    }

    @Override
    public void initHardware(HardwareMap hwMap, Telemetry telemetry) {
        this.telemetry = telemetry;

        int cameraMonitorViewId = hwMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hwMap.appContext.getPackageName());
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters(cameraMonitorViewId);

        //License key - do not change
        parameters.vuforiaLicenseKey = "ATg/rBr/////AAAAGZnKeJwS8EsbmCHOZbYu8wJblg6NqKxSlkFsSpcfk/BWpZvXcetFi9nDRvFCLArt9yqiGKF888pPGjmk6Iax3jrK+WqeE4eKi+z1eNMgYPIns8vtcvv/5Ol0RRxs5HsficO2AkIK1ek6+N8jCfFQdPsoIiS3L5Ykeh8huE8or4HllaLBKGP8IJQoeblu3r1C99GZeL0DpRtNHkSoo5Prg/8PNHvu2bWUuIE9dI9w6GKSGZ793vLuDIV2KAI2toKA1BQcwYG6eLa3dnKQXltjodhZl7u1s0OoJx/9wxDQNXM6V/QdYnW8euH/LopkWe4iieqQETCzlABaBdKio+YDvf2GksvhkcfMS6E8ax040a3T";

        parameters.cameraDirection = VuforiaLocalizer.CameraDirection.BACK;
        this.vuforia = ClassFactory.createVuforiaLocalizer(parameters);

        relicVuMark = this.vuforia.loadTrackablesFromAsset("RelicVuMark");
        relicTemplate = relicVuMark.get(0);
        relicTemplate.setName("relicVuMarkTemplate");

        relicVuMark.activate();
    }
}
