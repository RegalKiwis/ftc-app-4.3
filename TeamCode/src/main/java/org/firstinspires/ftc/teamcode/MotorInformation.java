package org.firstinspires.ftc.teamcode;

/**
 * Interface for reporting motor information.  Each motor in use by the
 * gets its own class that implements this interface.
 */
public interface MotorInformation {

    /**
     * @return The number of encoder pulses per motor revolution.
     */
    public double encoderCount();
    public double RPM();
    public double RPS();
}
