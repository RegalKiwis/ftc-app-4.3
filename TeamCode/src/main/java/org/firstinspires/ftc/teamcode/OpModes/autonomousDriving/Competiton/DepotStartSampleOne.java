package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.Competiton;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.segmentPaths;

/**
 * Autonomous program for driving through the depot to the crater without sampling.  Either alliance.  Starts
 * opposite from the depot in the lander area.
 */
@Autonomous(name = "Depot Start One Sample", group = "RED")
public class DepotStartSampleOne extends segmentPaths {
    @Override
    public void runOpMode() {
        //Autonomous opMode settings go in "onPlay"
        onPlay();

        //Put code to run here
        driveToDepot(true, true);
        depotToCrater(true, false);
    }
}