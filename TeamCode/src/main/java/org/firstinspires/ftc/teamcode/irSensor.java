package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.IrSeekerSensor;

import org.firstinspires.ftc.robotcore.external.Telemetry;

/**
 * Wrapper class for the irSensor that adds a pre-written hardware initialization method and telemetry method.
 */
public class irSensor implements Hardware {
    private IrSeekerSensor irSeeker;
    private Telemetry telemetry;

    @Override
    public void initHardware(HardwareMap hwMap, Telemetry telemetry) {
        irSeeker = hwMap.irSeekerSensor.get("sensor_ir");
        this.telemetry = telemetry;
    }

    @Override
    public void telemetryData() {
        // Ensure we have a IR signal
        if (irSeeker.signalDetected())
        {
            // Display angle and strength
            telemetry.addData("Angle",    irSeeker.getAngle());
            telemetry.addData("Strength", irSeeker.getStrength());
        }
        else
        {
            // Display loss of signal
            telemetry.addData("Seeker", "Signal Lost");
        }
    }

    public IrSeekerSensor getIrSeeker() {
        return irSeeker;
    }
}
