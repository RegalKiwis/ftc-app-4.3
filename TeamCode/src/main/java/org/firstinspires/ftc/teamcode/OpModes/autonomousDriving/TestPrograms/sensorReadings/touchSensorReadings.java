package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.TestPrograms.sensorReadings;

import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.OpModes.DriverControlledBase;

@TeleOp(name = "Touch Sensor Readings", group = "Sensor")
public class touchSensorReadings extends DriverControlledBase {
    @Override
    public void loop() {
        robot.touchSensor().telemetryData();
    }
}
