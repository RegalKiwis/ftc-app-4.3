package org.firstinspires.ftc.teamcode;

import android.graphics.Color;

import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.ScannedDevices;
import com.qualcomm.robotcore.util.RobotLog;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;
import org.firstinspires.ftc.teamcode.OpModes.Colors;

public class DistColorSensor implements Hardware {
    private ColorSensor colorSensor;
    private ColorSensor colorSensor2;
    private DistanceSensor distanceSensor;
    private DistanceSensor distanceSensor2;

    /**The difference between the reading of the grey foam tiles white tape.
     These must be updated whenever the optical distance sensor is moved because different amounts of light
     will be reported from different places on the robot based on the sensor's distance from the floor
     and the amount of external light.
     */
    private final double floorTapeDifference_WHITE = 0.38;

    //Telemetry
    private Telemetry telemetry;

    @Override
    public void initHardware(HardwareMap hwMap, Telemetry telemetry) {
        try {
            String name = "colorRangeSensor";

            colorSensor = hwMap.get(ColorSensor.class, name + 1);
            colorSensor.enableLed(true); //Doesn't do anything with Rev Color Sensor
            distanceSensor = hwMap.get(DistanceSensor.class, name+1);

            colorSensor2 = hwMap.get(ColorSensor.class, name + 2);
            colorSensor2.enableLed(true); //Doesn't do anything with Rev Color Sensor
            distanceSensor2 = hwMap.get(DistanceSensor.class, name+2);

            this.telemetry = telemetry;
        } catch (NullPointerException nullColorSense) {
            RobotLog.aa("DistColorSensor", "Hardware initialization failed");
            throw new NullPointerException();
        }
    }

    private float sumHSV = 0;
    private int numHSV = 0;
    private int numToAvg = 2;

    private float updateHueAvg(float newReading) {
        if (numHSV == numToAvg) {
            sumHSV = 0;
            numHSV = 0;
        }
        sumHSV += newReading;
        numHSV++;
        return sumHSV / numHSV;
    }

    private float[] hsv() {
        double SCALE_FACTOR = 255;
        float[] hsvValues = {0f, 0f, 0f};
        for (int i = 0; i < numToAvg; i++) {
            Color.RGBToHSV((int) ((colorSensor.red() * SCALE_FACTOR + colorSensor2.red() * SCALE_FACTOR) / 2.0),
                    (int) ((colorSensor.green() * SCALE_FACTOR + colorSensor2.green() * SCALE_FACTOR) / 2.0),
                    (int) ((colorSensor.blue() * SCALE_FACTOR + colorSensor2.blue() * SCALE_FACTOR) / 2.0),
                    hsvValues);
            hsvValues[0] = updateHueAvg(hsvValues[0]);
        }
        return hsvValues;
    }

    private Colors detectedColor() {
        if (hsv()[0] < 33 && hsv()[0] > 17) {
            return Colors.GOLD;
        }
        return Colors.WHITE;
    }

    public boolean detectColor(Colors color) {
        return detectedColor().equals(color);
    }

    public boolean detectColor() {
        return !this.detectColor(Colors.NONE);
    }

    public int alpha() {
        return colorSensor.alpha();
    }

    /**
     * @return The average of the two distances reported by the two Rev color range sensors located on the robot.
     */
    private double getDist() {
        return (distanceSensor.getDistance(DistanceUnit.INCH) + distanceSensor2.getDistance(DistanceUnit.INCH)) / 2.0;
    }

    /**
     * @return Determines whether an object is present using the range sensor function.  It reports true
     * as long as the distance reported is not "Not a Number/NAN".
     */
    public boolean objectDetected() {
        return getDist() != DistanceUnit.infinity;
    }

    /**
     * The ideal reading from the optical distance sensor for maintaining alignment with the
     * edge between the white tape and the grey foam tile.
     */
    public double proportionalPower(double power) {
        double edgeReading_WHITE = floorTapeDifference_WHITE / 2;
        return colorSensor.alpha() - edgeReading_WHITE + power;
    }

    @Override
    public void telemetryData() {
        telemetry.addLine("Color Detected? " + detectedColor());
        telemetry.addLine("Color Sensor Raw Readings:");
        telemetry.addData("Distance", getDist());
        telemetry.addData("Red", this.colorSensor.red());
        telemetry.addData("Green", this.colorSensor.green());
        telemetry.addData("Blue", this.colorSensor.blue());
        telemetry.addData("Alpha (brightness)", this.colorSensor.alpha());
        telemetry.addData("Hue", hsv()[0]);
    }
}
