package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.TestPrograms;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.*;

@Autonomous(name = "Structure Test", group = "Testing")
public class autonomousStructureTest extends segmentPaths {
    @Override
    public void runOpMode() throws InterruptedException {
        onPlay();

        while (opModeIsActive()) {
            telemetry.addLine("OpMode is running");
            telemetry.addData("Current Time:", getRuntime());
            telemetry.update();
        }
    }
}
