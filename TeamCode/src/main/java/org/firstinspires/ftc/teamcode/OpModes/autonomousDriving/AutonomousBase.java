package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.OpModes.Colors;
import org.firstinspires.ftc.teamcode.RobotHardware;


/**
 * LinearOpMode that contains a robot object & methods related to the timing of actions it is necessary to have classes with different functions used during the autonomous period jewel this class and each other
 * so that information specific to an instance of the LinearOpMode class continues to be accurate and consistent (e.g. the imuDrive & driveToTape classes require the OpModeIsActive boolean, but if each extended
 * separately from each class and then were instantiated as objects in robot hardware, then they would no longer refer to the LinearOpMode being run during the autonomous period).
 * <p>
 *     This does break down the is-a relationship (obviously an imuDrive is not an encoderDrive...) and would typically be silly; however, there was not another solution available except passing "this.opModeIsActive" from the currently running autonomous into every method call for driving
 *     the robot during the autonomous period, and nothing of significance is lost since all code will be working with the same set of code at all times negating any potential re-usability advantages, with only the drive train changing or additional class members being added.
 * </p>
 */
@Disabled
 public abstract class AutonomousBase extends LinearOpMode {
    //Create robot object using the hardware in class RobotHardware
    protected RobotHardware robot = new RobotHardware();

    //Our alliance color
    private Colors alliance;
    /**
     * The default timeout value in seconds used to abort robot movements (turning and driving) after they have taken greater than this amount of time
     */
    int getDefaultTimeOutS() {
        return 30;
    }

    public Colors alliance() {
        return alliance;
    }

    //Should be run at the start of each autonomous opMode

    /**
     * Method called at the start of autonomous OpModes that initializes the robot
     * and sets the correct driving direction based on the input alliance.
     * @param alliance Which the alliance an autonomous is designed for
     * @param delayLength
     */
    protected void onPlay(Colors alliance, int delayLength) {
        robot.initRobot(hardwareMap, telemetry);

        robot.driveTrain().useDefaultAutonDirection();
        robot.driveTrain().forwardDrive();
        this.alliance = alliance; //Sets the alliance color
        this.robot.driveTrain().setIsAutonomous(true);

        //Calibrate the robot's sensors and get the initial optical distance sensor reading of the floor
        robot.calibrate();

        autonomousDriveDirection();

        //Wait for the play button to be pressed
        waitForStart();
        
        //Reset the timer for the amount of time the opMode has been running
        resetStartTime();
        robot.beginSensorReadings();
        actionDelay(delayLength);
    }

    protected void onPlay(int delayLength) {
        this.onPlay(Colors.NONE, delayLength);
    }

    protected void onPlay() {
        this.onPlay(Colors.NONE, 0);
    }

    /**
     * Decides which direction the robot should drive in based on the
     * current alliance color for autonomous.  Currently
     * red alliance sets the drive train forward and
     * backwards when on the blue alliance.
     */
    protected void autonomousDriveDirection() {
        if (alliance().equals(Colors.RED)) {
            robot.driveTrain().forwardDrive();
        } else if (alliance().equals(Colors.BLUE)) {
            robot.driveTrain().backwardDrive();
        } else {
        robot.driveTrain().forwardDrive();
        }
    }


    /*
   * *******************************************
   * Timers & Delays
   * *******************************************
   */
    /**
     * Converts the input number of seconds to milliseconds
     *
     * @param seconds
     * @return the number of milliseconds in the number of seconds entered
     */
    private long secToMilliSec(long seconds) {
        return seconds * 1000;
    }

    /**
     * Stops the further actions from taking place for the input number of seconds without stopping threads
     * <p/>
     * It gets the current time from the internal timer and adds the input amount of time onto a separate variable,
     * stopping further actions from taking place until the internal timer has reached the target amount of time.
     * <p/>
     *
     * @param seconds the number of seconds to delay further robot actions
     */
    public void actionDelay(double seconds) {
        double targetSeconds = timeOutTarget(seconds);
        telemetry.clearAll();
        while (opModeIsActive() && timeRemaining(targetSeconds)) {
            telemetry.addLine("Waiting until " + String.valueOf(seconds) + " seconds have passed.");
            telemetry.addLine("Time remaining: " + String.valueOf(targetSeconds - getRuntime()));
            telemetry.update();
            idle();
        }
    }

    public void actionDelay() {
        actionDelay(.5);
    }

    /**
     * Stops any further actions from taking place until the input number of seconds have passed during the autonomous period
     *
     * @param seconds the number of seconds to delay further robot actions
     */
    void delayUntilGameTime(double seconds) {
        telemetry.clearAll();
        while (opModeIsActive() && timeRemaining(seconds)) {
            telemetry.addLine("Waiting until " + String.valueOf(seconds) + " seconds have passed in the round.");
            telemetry.update();
            idle();
        }
    }

    /**
     * Boolean reporting whether the input time is still after the current time.  Generally used for
     * timeouts in autonomous actions.  The input time should be the overall run time when of the match
     * after which false should be returned rather than a number of seconds.  Should be used in conjunction
     * with timeOutTarget()
     * @param timeOutTarget
     * @return
     */
    boolean timeRemaining(double timeOutTarget) {
        return getRuntime() < timeOutTarget;
    }

    /**
     * Returns a the game time in seconds equal to
     * the current runtime for the round + the
     * value of timeOutS.  Should be used in conjunction with timeRemaining().
     * @param timeOutS The number of seconds that an action should time out after
     * @return The game time that would be reached when an action has run out of time
     */
    double timeOutTarget(double timeOutS) {
        return getRuntime() + timeOutS;
    }

    protected int startDelayDefault = 7;

    /*
   * *******************************************
   * Generic Movement
   * *******************************************
   */
    interface targetReachedTest {
        boolean test();
    }
}

