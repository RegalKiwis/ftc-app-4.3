package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.TestPrograms;

import org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.segmentPaths;

@com.qualcomm.robotcore.eventloop.opmode.Autonomous(name = "Color Servo Test", group = "Testing")
public class ColorServoTest extends segmentPaths {
    @Override
    public void runOpMode() {
        onPlay();

        robot.colorServo().extend();
        driveUntilGold();
    }
}
