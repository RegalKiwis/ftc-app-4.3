package org.firstinspires.ftc.teamcode.DriveTrain.LinearSlide;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.segmentPaths;

/**
 * This lowers the linear slide by the distance needed to retract the slide from its extended position.
 */
@Autonomous(name = "Lower Slide", group = "SLIDE")
public class LowerSlide extends segmentPaths {
    @Override
    public void runOpMode() {
        //Autonomous opMode settings go in "onPlay"
        onPlay();

        //Put code to run here
        robot.linearSlideMotor().lower();
    }
}