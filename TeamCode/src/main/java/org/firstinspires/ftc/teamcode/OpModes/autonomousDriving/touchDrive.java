package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;


@Disabled
/**
 * Class containing methods for driving the robot forward until the touch sensor gives a positive reading (is pressed).
 * The touch sensor is currently assumed to be of a type such as the one made by Modern Robotics that will give a positive reading the moment it is pressed
 * rather than a variable reading depending on the level of depression (i.e. it either is pressed, or it is not pressed).
 */
abstract class touchDrive extends odsDrive {
    public void touchDrive(int timeoutS, double speed) {
        float initHeading = 0;
        //Telemetry
        robot.touchSensor().telemetryData();
        //Get timeout time
        double targetTime = timeOutTarget(timeoutS);
        //Begin robot movement
        robot.driveTrain().setCurrentSpeed(speed);
        while (opModeIsActive() && !robot.touchSensor().isPressed() && timeRemaining(targetTime)) {
            telemetry.update();
            idle();
        }
        robot.stopRobot();
    }

    public void touchDrive(int timeoutS) {
        this.touchDrive(timeoutS, robot.driveTrain().getAutonomousDriveSpeed());
    }

    public void touchDrive(double speed) {
        this.touchDrive(getDefaultTimeOutS(), speed);
    }

    public void touchDrive() {
        this.touchDrive(getDefaultTimeOutS(), robot.driveTrain().getAutonomousDriveSpeed());
    }
}
