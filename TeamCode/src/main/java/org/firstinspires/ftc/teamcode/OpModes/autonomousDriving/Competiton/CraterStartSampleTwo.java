package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.Competiton;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.segmentPaths;

/**
 * Autonomous program for driving through the depot to the crater without sampling.  Either alliance.  Starts
 * opposite from the depot in the lander area.
 */
@Autonomous(name = "Crater Start Sample Two", group = "RED")
public class CraterStartSampleTwo extends segmentPaths {
    @Override
    public void runOpMode() {
        //Autonomous opMode settings go in "onPlay"
        onPlay();

        //Put code to run here
        driveToDepot(false, true);
        depotToCrater(false, true);
    }
}