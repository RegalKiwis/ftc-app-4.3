package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.TestPrograms;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.segmentPaths;

/**
 * Unlike other test OpModes for testing the mineral scoring sequence, this
 * one does not include the preceding steps of driving to the mineral.  This
 * way testing can be completed without the entirety of the field being setup and with only the minerals set
 * at the correct spacing. The steps assume the robot begins by facing the center mineral as if
 * it had begun on the side of the lander opposite the crater.
 */
@Autonomous(name = "Test Mineral Scoring (Crater Start) - Color Sensor Aligned At Start", group = "TEST")
public class MineralScoreSequenceTestCrater extends segmentPaths {
    @Override
    public void runOpMode() {
        //Autonomous opMode settings go in "onPlay"
        onPlay();

        //Put code to run here
        scoreFirstMineral(false, false);
    }
}