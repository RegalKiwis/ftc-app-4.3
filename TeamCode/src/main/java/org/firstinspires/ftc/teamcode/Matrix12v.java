package org.firstinspires.ftc.teamcode;


public class Matrix12v implements MotorInformation {
    public double encoderCount() {
        return 1478.4;
    }

    public double RPM() {
        return 160;
    }

    public double RPS() {
        return RPM() / 60;
    }
}
