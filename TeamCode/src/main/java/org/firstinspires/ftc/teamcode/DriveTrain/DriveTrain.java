package org.firstinspires.ftc.teamcode.DriveTrain;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.util.Range;
import com.qualcomm.robotcore.util.RobotLog;

import static org.firstinspires.ftc.teamcode.RangeControl.*;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;
import org.firstinspires.ftc.teamcode.DcMotorControl;
import org.firstinspires.ftc.teamcode.Hardware;
import org.firstinspires.ftc.teamcode.RevHDHex_40Gear;
import org.firstinspires.ftc.teamcode.Directions;
import org.firstinspires.ftc.teamcode.MotorInformation;
import org.firstinspires.ftc.teamcode.RangeControl;

import java.util.ArrayList;

/**
 * Abstract class that contains methods for all drive train styles.  All drive train classes should jewel this class.
 * Motors must be added to the array storing the motors according to the order specified below.
 * <p/>
 * First: left back motor (Motor Array Index = 0)
 * <p/>
 * Second: right back motor (Motor Array Index = 1)
 * <p/>
 * Third: left front motor (Motor Array Index = 2)
 * <p/>
 * Fourth: right front motor (Motor Array Index = 3)
 * <p/>
 * <p>
 *     All motors used in the drive train are from the same manufacturer & that no more than four drive motors are in use.
 * </p>
 * If the standard driving direction of the robot is changed, then method implementations inside subclasses (e.g. tankDrive) should be updated
 * to reflect that rather than changing motor Directions throughout other places in the code.
 */
public abstract class DriveTrain implements Hardware {
    int numberOfMotors; //The total number of motors in the drive train
    private DcMotor motors[];
    private ArrayList<DcMotor> leftMotors = new ArrayList<>(); //Stores only the left motors
    private ArrayList<DcMotor> rightMotors = new ArrayList<>(); //Stores only the right motors
    private MotorInformation motorInfo = new RevHDHex_40Gear();

    private int startingPosition;

    private boolean isAutonomous = false;

    //Storage of the robot's current driving direction
    private Directions drivingDirection;

    public Directions getDrivingDirection() {
        return drivingDirection;
    }

    private final Directions autonomousDriveDirection = Directions.FORWARD;
    public void useDefaultAutonDirection() {
        this.setDirection(autonomousDriveDirection);
    }

    //Telemetry
    private Telemetry telemetry;

    public void setTelemetry(Telemetry telemetry) {
        this.telemetry = telemetry;
    }

    //Information related to driving using encoders
    private final double encoderCountPerRotation = motorInfo.encoderCount();
    private final double wheelDiameter = 90.0 * 0.0394; //Inches (90mm)
    private final double gearRatio = 1; // This is < 1.0 if geared UP
    private final double wheelCircumference = wheelDiameter * Math.PI;

    private final double encoderCountPerInch = (encoderCountPerRotation * gearRatio) /
            (wheelCircumference);

    public MotorInformation motorUsed() {
        return motorInfo;
    }

    //Driving Speeds for the robot
    /**
     * The fastest raw speed/motor power that any of the drive train motors
     * should be allowed to move at.  This is enforced by the method used for setting the drive train
     * motors' speeds, which will clip power values to be within this variable's value.
     * @see DriveTrain#setMaximumDriveSpeed(double)
     * @see DriveTrain#setCurrentSpeed(double)
     */
    private double maximumDriveSpeed;
    /**
     * The raw speed/motor power of drive train motors that is desired to be used during the autonomous period when driving by default.
     * Overloaded methods for completing autonomous tasks that do not receive a power value as a parameter
     * use this value by default.
     * @see DriveTrain#setAutonomousDriveSpeed(double)
     */
    public double autonomousDriveSpeed;
    /**
     * The raw speed/motor power of drive train motors that is desired to be used during the autonomous period when driving when an
     * additional, slower speed is desired at times during the autonomous period.
     * @see DriveTrain#setAutonomousDriveSpeed(double)
     */
    private double slowerAutonomousSpeed;
    /**
     * The raw speed/motor power that the drive motors should not go below in cases when
     * a minimum non-zero power value is desired.  An example would be when calculating the power
     * of the drive train motors based on the remaining distance to a target, and going below a certain
     * value would cause the robot to be unable to move.  This value has no effect on driver controlled
     * period driving; it only affects the power value when a power value is clipped using the boundedSpeed method and then passed
     * to the setCurrentSpeed method.
     * @see DriveTrain#boundedSpeed(double)
     * @see DriveTrain#setCurrentSpeed(double)
     * @see DriveTrain#setAutonomousDriveSpeed(double)
     */
    private double minSpeed;
    //Turning speeds
    private double turnSpeed, slowerTurnSpeed;

    public void setIsAutonomous(boolean isAutonomous) {
        this.isAutonomous = isAutonomous;
    }

    /**
     * Returns the other pre-set turning speed for use in autonomous.
     * Primarily for use by turning with a gyroscope
     * because a slower speed is needed than for turning with
     * motor encoders.
     * @return
     */
    public double getSlowerTurnSpeed() {

        return slowerTurnSpeed;

    }

    /**
     * Sets the other pre-set turning speed for use in autonomous.
     * Primarily for use by turning with a gyroscope
     * because a slower speed is needed than for turning with
     * motor encoders.
     */
    public void setSlowerTurnSpeed(double slowerTurnSpeed) {
        this.slowerTurnSpeed = Range.clip(slowerTurnSpeed, 0, 1);
    }

    public DcMotor[] getMotors() {
        return motors;
    }

    void setMotors(DcMotor... motors) {
        this.motors = motors;
    }

    public DcMotor[] getLeftMotors() {
        return leftMotors.toArray(new DcMotor[leftMotors.size()]);
    }

    public DcMotor[] getRightMotors() {
        return rightMotors.toArray(new DcMotor[rightMotors.size()]);
    }

    public double getTurnSpeed() {
        return turnSpeed;
    }

    public void setTurnSpeed(double turnSpeed) {
        this.turnSpeed = Range.clip(turnSpeed, 0, 1);
    }

    /**
     * Initiates the hardware being used in the drive train for use in the hardware map.
     * @param hwMap
     */
    @Override
    public void initHardware(HardwareMap hwMap, Telemetry telemetry) {
        try {
            for (int i = 0; i < motors.length; i++) {
                if (i % 2 == 0) { //Checks for left motors
                    motors[i] = hwMap.dcMotor.get(motorName(i));
                    leftMotors.add(motors[i]);
                } else { //Assumes right motor in other cases
                    motors[i] = hwMap.dcMotor.get(motorName(i));
                    rightMotors.add(motors[i]);
                }
            }
        } catch (NullPointerException nullDrive) {
            RobotLog.aa("DriveTrain", "Hardware initialization failed");
            throw new NullPointerException();
        }
        DcMotorControl.setMode(DcMotor.RunMode.RUN_USING_ENCODER, motors); //Use encoders by default so that the PID is used
        startingPosition = getMotors()[0].getCurrentPosition();
        DcMotorControl.setZeroBehave(DcMotor.ZeroPowerBehavior.BRAKE, motors);
        this.telemetry = telemetry;
    }

    /**
     * Handles the logic for creating a useful string name to be assigned to drive motors in the
     * hardware map by following the standard established for the class.
     * @param position The placement of the drive motor on the robot as a numerical value matching the order established in the class documentation
     * @see DriveTrain
     */
    public String motorName(int position) {
        boolean isBackMotor = position < 2;
        if (position % 2 == 0) {
            return isBackMotor ? "leftBackMotor" : "leftFrontMotor";
        } else {
            return isBackMotor ? "rightBackMotor" : "rightFrontMotor";
        }
    }

    /**
     * Sets the rotation of the drive motors so the robot moves forwards (assuming positive power values)
     * @see DcMotorSimple.Direction
     * @see DcMotorSimple#setDirection(com.qualcomm.robotcore.hardware.DcMotorSimple.Direction)
     */
    public void forwardDrive() {
        this.drivingDirection = Directions.FORWARD;
    }

    /**
     * Sets the rotation of the drive motors so the robot moves backwards (assuming positive power values)
     * @see DcMotorSimple.Direction
     * @see DcMotorSimple#setDirection(com.qualcomm.robotcore.hardware.DcMotorSimple.Direction)
     */
    public void backwardDrive() {
        this.drivingDirection = Directions.BACKWARD;
    }

    /**
     * Changes the direction of the drive motors so that the robot will rotate CCW (Left) given equal positive power to both drive motors
     * @see DcMotorSimple.Direction
     * @see DcMotorSimple#setDirection(com.qualcomm.robotcore.hardware.DcMotorSimple.Direction)
     */
    public void rotateCCW() {
        this.drivingDirection = Directions.LEFT;
    }

    /**
     * Changes the direction of the drive motors so that the robot will rotate CW (Right) given equal positive power to both drive motors
     * @see DcMotorSimple.Direction
     * @see DcMotorSimple#setDirection(com.qualcomm.robotcore.hardware.DcMotorSimple.Direction)
     */
    public void rotateCW() {
        this.drivingDirection = Directions.RIGHT;
    }

    /**
     * Returns a boolean indicating whether the drive motors have successfully reset their encoders (i.e. their encoder counts are zero)
     *
     * @return True if drive motors' encoders have a position of zero
     */
    public boolean driveEnocdersReset() {
        return DcMotorControl.encodersReset(motors);
    }

    public void resetEncoders() {
        DcMotorControl.resetEncoders(getMotors());
    }

    public void setCurrentSpeed(double power) {
        this.setCurrentSpeed(power, minSpeedSetting.IGNORE_MIN_SPEED);
    }

    public enum minSpeedSetting{ USE_MIN_SPEED, IGNORE_MIN_SPEED }

    /**
     * Sets  the power level of the left and right drive motors to the input.
     * If the input is greater than the maximum power, then the motors are set to run at the maximum power level instead.
     * @param power The power level for the drive train motors to move at
     * @param minSpeedToggle Controls whether the speed will be clipped to not be below the minimum speed set for the drive train
     * @see DriveTrain#maximumDriveSpeed
     * @see DriveTrain#speedBoundMax(double)
     */
    public void setCurrentSpeed(double power, minSpeedSetting minSpeedToggle) {
        switch (minSpeedToggle) {
            case USE_MIN_SPEED:
                DcMotorControl.setPower(boundedSpeed(power), getMotors());
            case IGNORE_MIN_SPEED:
                DcMotorControl.setPower(speedBoundMax(power), getMotors());
        }
    }


    /**
     * This overloaded version of the method defaults to ignoring the minimum speed setting.
     * @param power
     * @see DriveTrain#setCurrentLeftSpeed(double, org.firstinspires.ftc.teamcode.DriveTrain.DriveTrain.minSpeedSetting)
     */
    public void setCurrentLeftSpeed(double power) {
        this.setCurrentLeftSpeed(speedBoundMax(power), minSpeedSetting.IGNORE_MIN_SPEED);
    }

    /**
     * Sets  the power level of the left drive motor(s) to the input.
     * If the input is greater than the maximum power, then the motor(s) are set to run at the maximum power level instead.
     * @param power The power level for the drive train motor(s) to move at
     * @param minSpeedToggle Controls whether the speed will be clipped to not be below the minimum speed set for the drive train
     * @see DriveTrain#maximumDriveSpeed
     * @see DriveTrain#speedBoundMax(double)
     */
    public void setCurrentLeftSpeed(double power, minSpeedSetting minSpeedToggle) {
        switch (minSpeedToggle) {
            case USE_MIN_SPEED:
                DcMotorControl.setPower(boundedSpeed(power), getLeftMotors());
            case IGNORE_MIN_SPEED:
                DcMotorControl.setPower(speedBoundMax(power), getLeftMotors());
        }
    }

    /**
     * This overloaded version of the method defaults to ignoring the minimum speed setting.
     * @param power
     * @see DriveTrain#setCurrentRightSpeed(double, org.firstinspires.ftc.teamcode.DriveTrain.DriveTrain.minSpeedSetting)
     */
    public void setCurrentRightSpeed(double power) {
        this.setCurrentRightSpeed(speedBoundMax(power), minSpeedSetting.IGNORE_MIN_SPEED);
    }

    /**
     * Sets  the power level of the right drive motor(s) to the input.
     * If the input is greater than the maximum power, then the motor(s) are set to run at the maximum power level instead.
     * @param power The power level for the drive train motor(s) to move at
     * @param minSpeedToggle Controls whether the speed will be clipped to not be below the minimum speed set for the drive train
     * @see DriveTrain#maximumDriveSpeed
     * @see DriveTrain#speedBoundMax(double)
     */
    public void setCurrentRightSpeed(double power, minSpeedSetting minSpeedToggle) {
        switch (minSpeedToggle) {
            case USE_MIN_SPEED:
                DcMotorControl.setPower(boundedSpeed(power), getRightMotors());
            case IGNORE_MIN_SPEED:
                DcMotorControl.setPower(speedBoundMax(power), getRightMotors());
        }
    }

    /**
     * Clips the input motor power so that it is >= the minimum speed set for the robot
     * and <= the maximum speed set for the robot or their inverses.  This means a negative power input will
     * return a negative power but clipped within the inverse range of the robot's maximum and minimum speeds.
     * @see DriveTrain#minSpeed
     * @see DriveTrain#maximumDriveSpeed
     * @see RangeControl#rangeInBounds(double, double, double)
     * @param power The power value being clipped
     * @return The power value clipped to be >= the robot's minimum speed and <= the maximum speed
     */
    private double boundedSpeed(double power) {
        double boundedSpeed = rangeInBounds(power, this.minSpeed, this.maximumDriveSpeed);
        return Range.clip(boundedSpeed, -1, 1);
    }

    /**
     * Clips the input motor power so that it is <= the maximum speed set for the robot or its inverses.
     * This means a negative power input will return a negative power
     * but clipped within the inverse of the robot's maximum speed.
     * @see DriveTrain#maximumDriveSpeed
     * @see RangeControl#rangeCorrectMax(double, double)
     * @param power The power value being clipped
     * @return The power value clipped to be <= the maximum drive train speed
     */
    private double speedBoundMax(double power) {
        if (isAutonomous) {
            return power;
        } else {
            return rangeCorrectMax(power, this.maximumDriveSpeed);
        }
    }

    public double getMaximumDriveSpeed() {
        return maximumDriveSpeed;
    }

    /**
     * Sets the fastest motor speed that the drive train motors utilize
     */
    public void setMaximumDriveSpeed(double maxDriveSpeed) {
        this.maximumDriveSpeed = Range.clip(maxDriveSpeed, 0, 1);
    }

    public double getAutonomousDriveSpeed() {
        return this.autonomousDriveSpeed;
    }

    /**
     * Sets the motor speed that the drive train motors utilize by default during the autonomous period
     */
    public void setAutonomousDriveSpeed(double autonomousDriveSpeed) {
        this.autonomousDriveSpeed = autonomousDriveSpeed;
    }

    public double getSlowerAutonomousSpeed() {
        return slowerAutonomousSpeed;
    }

    /**
     * Sets the slower motor speed that the drive train motors utilize when moving more slowly during the autonomous period
     */
    public void setSlowerAutonomousSpeed(double slowerAutonomousSpeed) {
        this.slowerAutonomousSpeed = slowerAutonomousSpeed;
    }

    public double getMinSpeed() {
        return this.minSpeed;
    }

    /**
     * Sets the lowest motor speed that the drive train motors will utilize when
     * making approaches to the stopping point during the autonomous period.
     */
    public void setMinSpeed(double minSpeed) {
        this.minSpeed = minSpeed;
    }

    public void setDirection(Directions driveDirection) {
        switch (driveDirection) {
            case FORWARD:
                forwardDrive();
            case BACKWARD:
                backwardDrive();
            case LEFT:
                rotateCCW();
            case RIGHT:
                rotateCW();
        }
    }

    public void reverseDirection() {
        this.setDirection(this.drivingDirection.opposite);
    }

    public movementCalc calculateMovements = new movementCalc();

    public class movementCalc {
        public double distanceRemaining(int targetEncoderCount, DistanceUnit unit) {
            return unit.fromInches(Math.abs((targetEncoderCount - getMotors()[0].getCurrentPosition()) / encoderCountPerInch));
        }

        public double distanceRemaining(double targetEncoderCount) {
            return distanceRemaining((int) targetEncoderCount, DistanceUnit.INCH);
        }

        public double distanceMoved(double inchesToMove, int targetEncoderCount, DistanceUnit unitReturn) {
            return unitReturn.fromInches(inchesToMove - distanceRemaining(targetEncoderCount));
        }

        public double distanceMoved(double inchesToMove, int targetEncoderCount) {
            return distanceMoved(inchesToMove, targetEncoderCount, DistanceUnit.INCH);
        }

        public int inchesToEncoderCount(double inches) {
            return (int) (inches * encoderCountPerInch);
        }

        public double encoderCountToInches(double encoderCount) {
            return encoderCount / encoderCountPerInch;
        }

        public double getEncoderTarget(double inches) {
            double encoderTarget = this.inchesToEncoderCount(inches);
            encoderTarget += motors[0].getCurrentPosition();
            return encoderTarget;
        }

    }

    public void increaseEncoderCount(int encoderMovement) {
        DcMotorControl.setTargetPositionRelative(encoderMovement, motors);
    }

    private double inchesMoved() {
        return calculateMovements.encoderCountToInches(Math.abs(getMotors()[0].getCurrentPosition() - startingPosition));
    }

    private void motorTelemetry(int motorIndex) {
        telemetry.addLine(motorName(motorIndex));
        telemetry.addData("Encoder Count", getMotors()[motorIndex].getCurrentPosition());

        telemetry.addLine("Port Information:");
        telemetry.addData("Speed", getMotors()[motorIndex].getPower());
        telemetry.addData("Port Number", getMotors()[motorIndex].getPortNumber());
        telemetry.addData("Device Information",getMotors()[motorIndex].getConnectionInfo());

        telemetry.addLine("_____________________________");
    }

    public void telemetryData() {
        for(int i = 0; i < getMotors().length; i++) {
            motorTelemetry(i);
        }
        telemetry.addData("Inches Moved", inchesMoved());
    }

    protected void debugTesting() {

        if (encoderCountPerInch <= 0) {
            throw new AssertionError("The calculation for the number of encoder counts per inch of movement is less than or " +
                    "equal to zero, indicating that values used in the calculation are missing or that the number of " +
                    "decimal places for the value exceeds the amount that can be stored by a number of type double.");
        } else if (maximumDriveSpeed <= 0 || maximumDriveSpeed > 1) {
            throw new AssertionError("The maximum driving speed is not between zero and one.");
        } else if (numberOfMotors % 2 != 0) {
            throw new AssertionError("An uneven number of motors is being used.  The DriveTrain class" +
                    "will need to be updated if this is desired.");
        } else if (numberOfMotors <= 0) {
            throw new AssertionError("There are not any detectable motors in the drive train constructor.");
        } else if (numberOfMotors > 4) {
            throw new AssertionError("You are trying to use greater than four drive train " +
                    "motors.  The DriveTrain class's motorName method will need to be updated to support this.");
        }

    }
}

