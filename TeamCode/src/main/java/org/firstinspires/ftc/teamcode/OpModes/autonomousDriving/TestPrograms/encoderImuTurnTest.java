package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.TestPrograms;

import org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.*;

@com.qualcomm.robotcore.eventloop.opmode.Autonomous(name = "Full Encoder + IMU Test", group = "Testing")
public class encoderImuTurnTest extends segmentPaths {
    @Override
    public void runOpMode() throws InterruptedException {
        onPlay();

        encoderDrive(drivingTestingDist);
        imuTurn(180);
        encoderDrive(drivingTestingDist);
        imuTurn(90);
        encoderDrive(drivingTestingDist);
        imuTurn(-90);
    }
}
