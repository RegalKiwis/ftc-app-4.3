package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.TestPrograms;

import org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.segmentPaths;

        import org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.*;

@com.qualcomm.robotcore.eventloop.opmode.Autonomous(name = "IMU Drive Test", group = "Testing")
public class imuDriveTest extends segmentPaths {
    @Override
    public void runOpMode() throws InterruptedException {
        onPlay();

        imuDrive(drivingTestingDist);
    }
}
