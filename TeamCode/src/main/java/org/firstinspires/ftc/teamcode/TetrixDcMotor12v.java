package org.firstinspires.ftc.teamcode;

public class TetrixDcMotor12v implements MotorInformation {
    public double encoderCount() {
        return 1440;
    }

    public double RPM() {
        return 152;
    }

    public double RPS() {
        return RPM() / 60;
    }
}
