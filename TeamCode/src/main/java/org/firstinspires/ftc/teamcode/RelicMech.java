package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;

import org.firstinspires.ftc.robotcore.external.Telemetry;

/**
 * Class with methods controlling the motor that powers the
 * mechanism on teh robot that is used for lifting and scoring the relics.
 * This motor powers two wheels that jewel the tape measure.
 */
public class RelicMech implements Hardware {
    private DcMotor relicMotor;
    private Telemetry telemetry;

    private final double power = .8;
    private final double wheelDiameter = 3.54331; //Inches (90mm)
    private final double wheelCircumference = Math.PI * wheelDiameter;
    private final double gearRatio = 120 / 90;
    private final double encoderCountPerInch = new RevHDHex_40Gear().encoderCount() / wheelCircumference * gearRatio;
    private final DcMotor.RunMode defaultRunMode = DcMotor.RunMode.RUN_WITHOUT_ENCODER;

    @Override
    public void initHardware(HardwareMap hwMap, Telemetry telemetry) {
        this.telemetry = telemetry;
        relicMotor = hwMap.dcMotor.get("relicMotor");

        relicMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        while (relicMotor.getCurrentPosition() != 0) {
            telemetry.addLine("Currently Resetting Relic Motor to Zero");
        }

        relicMotor.setMode(defaultRunMode);
        relicMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        //Positive power extends the tape measure
        relicMotor.setDirection(DcMotorSimple.Direction.FORWARD);
    }

    @Override
    public void telemetryData() {
        telemetry.addLine("Relic Motor Info");
        telemetry.addData("Current Power", relicMotor.getPower());
        telemetry.addData("Current Distance", currentDistance());
    }

    private double currentDistance() {
        return relicMotor.getCurrentPosition() / encoderCountPerInch;
    }

    public void moveDistance(double inches) {
        if (!relicMotor.isBusy()) { //Avoids continually extending by constantly setting the target position further away
            int targetEncoderCount = (int) (encoderCountPerInch * inches);
            targetEncoderCount += relicMotor.getCurrentPosition();
            relicMotor.setTargetPosition(targetEncoderCount);
            relicMotor.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            relicMotor.setPower(power);
            stopAtTarget();
        }
    }

    public void resetPosition() {
        relicMotor.setTargetPosition(0);
        relicMotor.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        relicMotor.setPower(power);

        stopAtTarget();
    }

    private void stopAtTarget() {
        //Designed as a state machine to maintain functionality during TeleOp
        if (!relicMotor.isBusy()) {
            relicMotor.setMode(defaultRunMode);
            relicMotor.setPower(0);
        }
    }

    public void setDirection(boolean forward) {
        if (forward) {
            relicMotor.setPower(power);
        } else {
            relicMotor.setPower(-power);
        }
     }

     public void stop() {
        relicMotor.setPower(0);
     }
}
