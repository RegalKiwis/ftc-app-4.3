package org.firstinspires.ftc.teamcode;

/**
 * This is an implementation of an exponential moving average
 * or low pass filter to reduce the amount of variance in calculating the
 * result of sensor readings.  The internal values are designed for use with the Adafruit IMU,
 * but it could be adjusted to work with other sensors.
 */
public class LowPassFilter {
    //Low pass filter implementation
    private double cutoffFrequency = 400;
    private double timeConstant = 1/(2 * Math.PI * cutoffFrequency);
    private double samplingRate;
    //private double alpha = samplingRate / (samplingRate + timeConstant);
    private final double alpha = .5;
    private Double oldValue;

    public LowPassFilter(double samplingRate) {
        this.samplingRate = samplingRate;
    }

    public double average(double value) {
        if (oldValue == null) {
            oldValue = value;
            return value;
        }
        double newValue = oldValue + alpha * (value - oldValue);
        oldValue = newValue;
        return newValue;
    }
}
