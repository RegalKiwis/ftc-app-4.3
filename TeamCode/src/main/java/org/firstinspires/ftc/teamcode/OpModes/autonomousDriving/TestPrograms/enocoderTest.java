package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.TestPrograms;

import org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.*;

@com.qualcomm.robotcore.eventloop.opmode.Autonomous(name = "Encoder Test", group = "Testing")
public class enocoderTest extends segmentPaths {
    @Override
    public void runOpMode() throws InterruptedException {
        onPlay();

        encoderDrive(drivingTestingDist);
    }
}
