package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.TestPrograms;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.OpModes.Colors;
import org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.segmentPaths;

/**
 *
 */
@Autonomous(name = "ODS Driving Test", group = "Testing")
public class odsDriveTest extends segmentPaths {
    @Override
    public void runOpMode() {
        onPlay();

        driveToTape(Colors.WHITE);
        actionDelay(1);

        alignWithTape();
    }
}
