package org.firstinspires.ftc.teamcode.OpModes;


/**
 * Driver controlled OpMode for the sole purpose of the testing the preset boundaries for the movement
 * of the servos on the robot using gamepad controls.
 */
@com.qualcomm.robotcore.eventloop.opmode.TeleOp(name = "Gamepad Servo Control", group = "TeleOp")
public class ServoMove extends DriverControlledBase {
    @Override
    public void loop() {
        markerClose();
        markerOpen();

        colorClose();
        colorOpen();

        driveRobot();
    }

}


