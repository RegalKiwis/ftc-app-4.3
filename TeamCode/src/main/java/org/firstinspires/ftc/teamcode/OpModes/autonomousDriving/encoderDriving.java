package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.hardware.DcMotor;

import org.firstinspires.ftc.teamcode.DcMotorControl;
import org.firstinspires.ftc.teamcode.DriveTrain.DriveTrain;
import org.firstinspires.ftc.teamcode.RobotHardware;

@Disabled
abstract class encoderDriving extends imuMovement {
    /**
     * Drives the robot the input number of inches using the motor encoders of the drive train.
     * A negative distance can be used to drive the robot backwards from the default autonomous direction.
     * Uses the RUN_TO_POSITION motor mode.
     * @param inches The distance in inches the robot should drive
     * @param power The motor power that drive train motors will move at during execution
     * @param timeOutS The number of seconds after which this method will cease and the robot will stop
     * @param sensorCorrect True if an integrated motion unit and the encoders should be combined to adjust the movement of the robot
     * @param  initHeading The robot's Z-Axis orientation when the method starts used for maintaining this initial alignment while driving
     */
    private void encoderDrive (double inches, double power, int timeOutS, boolean sensorCorrect, float initHeading) {
        int encoderMovement = robot.driveTrain().calculateMovements.inchesToEncoderCount(inches);
        DcMotorControl.setMode(DcMotor.RunMode.RUN_USING_ENCODER, robot.driveTrain().getMotors());

        robot.driveTrain().increaseEncoderCount(encoderMovement);
        //Use run to position mode for drive train motors
        DcMotorControl.setMode(DcMotor.RunMode.RUN_TO_POSITION, robot.driveTrain().getMotors()); //Sets the motors to the run to position mode

        double targetTime = timeOutTarget(timeOutS);
        robot.driveTrain().setCurrentSpeed(power);

        while (opModeIsActive() && !isStopRequested() && timeRemaining(targetTime) && DcMotorControl.allMotorBusy(robot.driveTrain().getMotors())) {
            if (sensorCorrect) {
                sensorCorrectedDriving(inches, robot.driveTrain().getMotors()[0].getTargetPosition(), power, initHeading);
            }
            robot.driveTrain().telemetryData();
            idle();
        }
        DcMotorControl.setMode(DcMotor.RunMode.RUN_USING_ENCODER, robot.driveTrain().getMotors());
        robot.stopRobot();
    }

    protected void encoderDrive(double inches, float initHeading) {
        this.encoderDrive(inches, robot.driveTrain().getAutonomousDriveSpeed(), getDefaultTimeOutS(), true, initHeading);
    }

    protected void encoderDrive(double inches, boolean correct) {
        this.encoderDrive(inches, robot.driveTrain().getAutonomousDriveSpeed(), getDefaultTimeOutS(), correct, robot.imu().currentOrientation());
    }

    protected void encoderDrive(double inches) {
        this.encoderDrive(inches, robot.driveTrain().getAutonomousDriveSpeed(), getDefaultTimeOutS(), true, robot.imu().currentOrientation());
    }

    /**
     * Turns the robot the given number of degrees using the RUN_TO_POSITION mode
     * of the drive train motors.  If turning is inaccurate, the robot's length front to back
     * should be updated in the RobotHardware class as this is used to calculate
     * the conversion between a given number of degrees and the inches of movement required
     * to reach the target.
     * +Angle is CCW (LEFT), and -angle is CW (RIGHT)
     * @param degrees
     * @param power
     * @param timeOutS
     */
    protected void encoderTurn(double degrees, double power, int timeOutS) {
        double inches = (Math.abs(degrees)/360.0) * robot.getRotationalCircumference();
        DcMotorControl.setMode(DcMotor.RunMode.RUN_USING_ENCODER, robot.driveTrain().getMotors());
        int encoderMovement = robot.driveTrain().calculateMovements.inchesToEncoderCount(inches);

        int leftMovement, rightMovement;
        if (degrees > 0) {
            leftMovement = -encoderMovement;
            rightMovement = encoderMovement;
        } else {
            leftMovement = encoderMovement;
            rightMovement = -encoderMovement;
        }

        DcMotorControl.setTargetPositionRelative(leftMovement, robot.driveTrain().getLeftMotors());
        DcMotorControl.setTargetPositionRelative(rightMovement, robot.driveTrain().getRightMotors());

        //Use run to position mode for drive train motors
        DcMotorControl.setMode(DcMotor.RunMode.RUN_TO_POSITION, robot.driveTrain().getMotors()); //Sets the motors to the run to position mode
        robot.driveTrain().setCurrentSpeed(power);

        robot.driveTrain().telemetryData();

        double targetTime = timeOutTarget(timeOutS);
        double currentPower;
        int changePower = 4;
        while (timeRemaining(targetTime) && opModeIsActive() && !isStopRequested() && DcMotorControl.allMotorBusy(robot.driveTrain().getMotors())) {
            if (changePower % 4 == 0) {
                currentPower = proportionalTurningPower(error(leftMovement, degrees), power, .02);
                robot.driveTrain().setCurrentSpeed(currentPower, DriveTrain.minSpeedSetting.USE_MIN_SPEED);
                changePower++;
            } else {
                changePower++;
            }

            telemetry.addData("Target angle", degrees);
            robot.driveTrain().telemetryData();
            telemetry.update();
            idle();
        }
        robot.stopRobot();
    }

    protected void encoderTurn(double degrees) {
        this.encoderTurn(degrees, robot.driveTrain().getTurnSpeed(), getDefaultTimeOutS());
    }

    private float error(int encoderMovement, double degrees) {
        double degreeMeasure = encoderMovement / degrees; //The number of encoder counts for moving one degree
        double remainingEncoderCount = Math.abs(robot.driveTrain().getMotors()[0].getTargetPosition() - robot.driveTrain().getMotors()[0].getCurrentPosition());
        return (float) (remainingEncoderCount / degreeMeasure);
    }
}
