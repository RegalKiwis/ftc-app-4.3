package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.TestPrograms.sensorReadings;

import org.firstinspires.ftc.teamcode.OpModes.DriverControlledBase;

@com.qualcomm.robotcore.eventloop.opmode.TeleOp(name = "ODS Readings", group = "Sensor")
public class odsReadings extends DriverControlledBase {
    @Override
    public void loop() {
        robot.ods().telemetryData();
    }
}
