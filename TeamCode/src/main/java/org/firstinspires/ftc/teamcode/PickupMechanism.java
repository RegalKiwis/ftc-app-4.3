package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.robotcore.external.Telemetry;

public class PickupMechanism implements Hardware {
    private Servo leftServo, rightServo;
    private boolean closed = false;

    private Telemetry telemetry;

    @Override
    public void telemetryData() {
        telemetry.addData("Glyph Mech State", closed ? "closed" : "open");
    }

    @Override
    public void initHardware(HardwareMap hwMap, Telemetry telemetry) {
        //Left and right are defined looking out from the back of the mechanism

        leftServo = hwMap.get(Servo.class, "leftServo");
        rightServo = hwMap.get(Servo.class, "rightServo");

        leftServo.setDirection(Servo.Direction.FORWARD);
        rightServo.setDirection(Servo.Direction.REVERSE);

        this.telemetry = telemetry;
    }

    public void close() {
        leftServo.setPosition(.2);
        rightServo.setPosition(.95);
    }

    private double leftOpen = .05, rightOpen = .85;

    public void open() {
        leftServo.setPosition(leftOpen);
        rightServo.setPosition(rightOpen);
    }

    public void wideOpen() {
        leftServo.setPosition(0);
        rightServo.setPosition(.75);
    }

    public void toggle(boolean narrow) {
        closed = !closed;
        if (closed) {
            if (narrow) {
                extraClose();
            } else {
                close();
            }
        } else {
            if (narrow) {
                open();
            } else {
                wideOpen();
            }
        }
    }

    public void extraClose() {
        leftServo.setPosition(.3);
        rightServo.setPosition(1);
    }

    /**
     * Method that opens each side of servos to avoid the markerServo glyph arms catching on each other.
     * The left markerServo opens and then the right markerServo opens.  This is based on a view from the front of the robot.
     */
    public void initialOpen() {
        rightServo.setPosition(rightOpen);
        long startTime = System.nanoTime() / 1000000000;
        while (System.nanoTime() / 1000000000 - startTime <= .4) { //One second delay
            telemetry.addLine("Currently Moving Servoes into initial position");
            telemetry.update();
        }
        leftServo.setPosition(leftOpen);
    }
}
