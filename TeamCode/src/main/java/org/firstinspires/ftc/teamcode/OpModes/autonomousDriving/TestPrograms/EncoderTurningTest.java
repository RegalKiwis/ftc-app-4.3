package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.TestPrograms;

import org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.*;

@com.qualcomm.robotcore.eventloop.opmode.Autonomous(name = "Encoder Turning Test", group = "Testing")
public class EncoderTurningTest extends segmentPaths {
    @Override
    public void runOpMode() {
        onPlay();
        robot.driveTrain().forwardDrive();
        encoderTurn(90);
    }
}
