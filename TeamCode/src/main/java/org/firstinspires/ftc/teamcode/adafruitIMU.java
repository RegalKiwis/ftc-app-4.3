package org.firstinspires.ftc.teamcode;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.hardware.bosch.JustLoggingAccelerationIntegrator;
import com.qualcomm.hardware.bosch.NaiveAccelerationIntegrator;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.util.RobotLog;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;
import org.firstinspires.ftc.robotcore.external.navigation.Position;
import org.firstinspires.ftc.robotcore.external.navigation.Velocity;

/**
 * This class adds to the existing functionality of  the Adafruit IMU's official FTC class and stores the speeds used while turning
 * When an object of this class is initialized, it will contain a declared BNO055IMU object named "imu".
 */

public class adafruitIMU implements Hardware {
    //The turns are considered complete when within +- of this value
    private final double turningTolerance = 2;
    private BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
    private BNO055IMU imu; //IMU object that will be created with the adafruitIMU object

    //Telemetry
    private Telemetry telemetry;

    //Constructor
    adafruitIMU() {
        // Set up the parameters with which we will use our IMU. Note that integration
        // algorithm here just reports accelerations to the logcat log; it doesn't actually
        // provide positional information.
        parameters.angleUnit = BNO055IMU.AngleUnit.DEGREES;
        parameters.accelUnit = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        parameters.calibrationDataFile = "AdafruitIMUCalibration.json"; // see the calibration sample opmode
        parameters.loggingEnabled = true;
        parameters.loggingTag = "IMU";
        parameters.accelerationIntegrationAlgorithm = new AccelerationIntegrator(100);
    }

    public BNO055IMU sensor() {
        return this.imu;
    }

    public double turningTolerance() {
        return this.turningTolerance;
    }

    public double getTurningTolerance() {
        return turningTolerance;
    }

    @Override
    public void initHardware(HardwareMap hwMap, Telemetry telemetry) {
        try {
            imu = hwMap.get(BNO055IMU.class, "imu");
            imuCalibrate();
            this.telemetry = telemetry;
        } catch (NullPointerException nullImu) {
            RobotLog.aa("adafruitIMU", "Hardware initialization failed");
            throw new NullPointerException();
        }
    }

    //Calibration
    void imuCalibrate() {
        //Hardware Initiation
        imu.initialize(parameters);
    }

    public void beginReadings() {
        long acquisitionTime = 100; //Milliseconds
        imu.startAccelerationIntegration(new Position(DistanceUnit.INCH, 0, 0, 0, acquisitionTime), new Velocity(), (int) acquisitionTime); //Updated ever .2 seconds to reduce the chance for overshooting targets
    }

    /**
     * @return the linear acceleration of the imu in inches/second squared
     */
    public double linearAcceleration() {
        final double meterToInchConversionFactor = 39.3701;
        return imu.getLinearAcceleration().xAccel * meterToInchConversionFactor; //See https://cdn-shop.adafruit.com/datasheets/BST_BNO055_DS000_12.pdf on page 24 (default forward axis is X)
    }

    /**
     * Gets the robot's current angle orientation
     *
     * @return The value of the Z-Axis orientation reading
     */
    public float currentOrientation() {
        //Returns the inverse of the orientation reading to make it positive in accordance with the FTC coordinate system
        return imu.getAngularOrientation().toAxesReference(AxesReference.INTRINSIC).toAxesOrder(AxesOrder.ZYX).firstAngle ;
    }

    public Position currentPosition() {
        return imu.getPosition().toUnit(DistanceUnit.INCH);
    }

    @Override
    public void telemetryData() {
        telemetry.addData("Current Angle: ", this.currentOrientation());
        telemetry.addData("Current Normalized Angle: ", AngleUnit.DEGREES.normalize(this.currentOrientation()));
        telemetry.update();
    }
}

