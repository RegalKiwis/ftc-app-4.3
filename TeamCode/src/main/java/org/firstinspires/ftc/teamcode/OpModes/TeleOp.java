package org.firstinspires.ftc.teamcode.OpModes;

@com.qualcomm.robotcore.eventloop.opmode.TeleOp(name = "Driver Controlled", group = "TeleOp")
/**
 * Controls the robot using user input from one gamepad.
 * Uses the hardware & variables stored in RobotHardware.
 *
 * Controls:
 *
 * Gamepad 1:
 * Gamepad 2:
 * Unused
 */
public class TeleOp extends DriverControlledBase {

}


