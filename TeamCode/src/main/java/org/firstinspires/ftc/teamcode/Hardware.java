package org.firstinspires.ftc.teamcode;
import com.qualcomm.robotcore.hardware.HardwareMap;
import org.firstinspires.ftc.robotcore.external.Telemetry;

/**
 * Contains standardized methods for implementation by hardware wrapper classes allowing for
 * pre-written and formatted telemetry & pre-written hardware initialization methods.
 * This allows the same method to be called by all robot hardware for the same purpose and
 * the use of a list in RobotHardware for initializing the hardware.
 */
public interface Hardware {
    public void initHardware(HardwareMap hwMap, Telemetry telemetry);
    public void telemetryData();
}
