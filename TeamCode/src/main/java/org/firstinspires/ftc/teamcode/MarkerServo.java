package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.robotcore.external.Telemetry;

/**
 * Wrapper class for using a markerServo arm through the shared Hardware interface.
 */
public class MarkerServo implements Hardware {
    private com.qualcomm.robotcore.hardware.Servo servo;
    private Telemetry telemetry;

    @Override
    public void initHardware(HardwareMap hwMap, Telemetry telemetry) {
        servo = hwMap.get(com.qualcomm.robotcore.hardware.Servo.class, "markerServo");

        //Set so that zero position has markerServo folded, which is why the direction is reversed
        servo.setDirection(com.qualcomm.robotcore.hardware.Servo.Direction.REVERSE);
        this.telemetry = telemetry;
    }

    @Override
    public void telemetryData() {
       telemetry.addData("Current markerServo position", servo.getPosition());
       telemetry.update();
    }

    public void extend() {
        servo.setPosition(1);
    }

    public void retract() {
        servo.setPosition(.91);
    }
}
