package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.TestPrograms.sensorReadings;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.segmentPaths;

/**
 * A program for testing the robot's lift.  It simulates the beginning of autonomous when the robot is lowered to the ground.
 */
@Autonomous(name = "LiftTest", group = "Test")
public class LiftTest extends segmentPaths {
    @Override
    public void runOpMode() {
        //Autonomous opMode settings go in "onPlay"
        onPlay();

        //Put code to run here
        lowerAndUnhookRobot();
    }
}