package org.firstinspires.ftc.teamcode.OpModes;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.Gamepad;

import org.firstinspires.ftc.teamcode.RobotHardware;

@Disabled
public abstract class DriverControlledBase extends OpMode {
    public final RobotHardware robot = new RobotHardware();
    private final double TRIGGER_PRESSED = 0.5; //Triggers pressed when >= this value
    private double originalMax = robot.driveTrain().getMaximumDriveSpeed();
    private double lastPressTime = getRuntime();

    //Trigger controls
    private boolean leftTriggerPressed(Gamepad gamepad) {
        return gamepad.left_trigger >= TRIGGER_PRESSED;
    }

    private boolean rightTriggerPressed(Gamepad gamepad) {
        return gamepad.right_trigger >= TRIGGER_PRESSED;
    }

    private void buttonControls() {
        robot.linearSlideMotor().togglePosition(gamepad1.x);
    }

    void markerOpen() {
        if (gamepad1.a)
            robot.getMarkerServo().extend();
    }

    void markerClose() {
        if (gamepad1.b)
            robot.getMarkerServo().retract();
    }

    void colorOpen() {
        if (gamepad1.x)
            robot.colorServo().extend();
    }

    void colorClose() {
        if (gamepad1.y)
            robot.colorServo().retract();
    }

    private void fastDrive() {
        robot.driveTrain().setMaximumDriveSpeed(originalMax);
    }

    private void slowDrive() {
        robot.driveTrain().setMaximumDriveSpeed(.25);
    }

    private void extraSlowDrive() {
        robot.driveTrain().setMaximumDriveSpeed(.15);
    }



    private void defaultState() {
        if (leftTriggerPressed(gamepad1)) {
            robot.pickupMechanism().open();
        } else if (rightTriggerPressed(gamepad1)) {
            robot.pickupMechanism().open();
        }
    }

    protected void driveRobot() {
        double leftStick = -gamepad1.left_stick_y;
        double rightStick = -gamepad1.right_stick_y;

        if (gamepad1.dpad_down && getRuntime() - lastPressTime < 1) {
            extraSlowDrive();
            lastPressTime = getRuntime();
        } else if (gamepad1.dpad_down) {
            slowDrive();
            lastPressTime = getRuntime();
        } else if (gamepad1.dpad_up) {
            fastDrive();
        }

        robot.driveTrain().setCurrentLeftSpeed(leftStick);
        robot.driveTrain().setCurrentRightSpeed(rightStick);
    }

    private boolean differentValues(double one, double two) {
        return  (one < 0 && two > 0) || (one > 0 && two < 0);
    }

    private boolean bothNeg(double one, double two) {
        return one < 0 && two < 0;
    }

    private void driverControlledPeriod() {
        driveRobot();
        buttonControls();

        //Telemetry data (Should be done after the sensors are calibrated)
        robot.driveTrain().telemetryData();
        telemetry.update();
    }

    @Override
    public void init() {
        //Robot hardware initialization
        robot.initRobot(hardwareMap, telemetry);
        robot.driveTrain().forwardDrive();
    }

    @Override
    public void start() {
        robot.getMarkerServo().retract();
        robot.colorServo().retract();
    }

    @Override
    public void loop() {
        driverControlledPeriod();
    }
}