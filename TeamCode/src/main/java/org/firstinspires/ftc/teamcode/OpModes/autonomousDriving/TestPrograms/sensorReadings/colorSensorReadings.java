package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.TestPrograms.sensorReadings;

import org.firstinspires.ftc.teamcode.OpModes.DriverControlledBase;

/**
 * Class that returns data from a color sensor such as RGB, HSV, and alpha values.
 */
@com.qualcomm.robotcore.eventloop.opmode.TeleOp(name = "Color Sensor Readings", group = "Sensor")
public class colorSensorReadings extends DriverControlledBase {
    @Override
    public void start() {
        robot.colorServo().extend();
    }

    @Override
    public void loop() {
        robot.colorServo().telemetryData();
        telemetry.update();
    }
}
