package org.firstinspires.ftc.teamcode.OpModes;

/**
 * This class stores turning values in degrees for using the integrated motion unit or a gyroscope to turn.
 * The class's primary purpose is to prevent turning values from manually being inverse for each alliance color.
 * Each alliance's side of the field is a mirror image of the other, so the robot needs to turn in the opposite Directions on each side to reach the same points.
 * which is prone to errors and wastes time.  The default alliance color is red (meaning the turning values that are input will be inverted when the alliance is blue).
 */
public class turningValues {
    private Colors alliance;
    private float turningValues[];
    //Negative one is used so that incrementing the value will make it zero, the first usable index value for the array of turning values
    private int turnCounter = -1;

    public turningValues(Colors alliance, float...turningValues) {
        this.alliance = alliance;
        this.turningValues = turningValues;
    }

    public void resetTurnCounter() {
        this.turnCounter = -1;
    }

    public void setAlliance(Colors alliance) {
        this.alliance = alliance;
    }

    private float turnNumber(int turnNumber) {
        switch (this.alliance) {
            case RED:
                return turningValues[turnNumber];
            case BLUE:
                return -turningValues[turnNumber];
            default:
                throw new AssertionError("An alliance color (Red or Blue) was not specified.");
        }
    }

    public float currentTurn() {
        turnCounter++;
        return turnNumber(turnCounter);
    }
}
