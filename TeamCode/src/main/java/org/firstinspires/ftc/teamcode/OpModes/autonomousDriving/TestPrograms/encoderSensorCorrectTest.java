package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.TestPrograms;

import org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.*;

@com.qualcomm.robotcore.eventloop.opmode.Autonomous(name = "Encoder IMU Corrected Test", group = "Testing")
public class encoderSensorCorrectTest extends segmentPaths {
    @Override
    public void runOpMode() throws InterruptedException {
        onPlay();

        encoderDrive(drivingTestingDist * 4, true); //The distance was increased to allow for easier testing of heading correction
    }
}
