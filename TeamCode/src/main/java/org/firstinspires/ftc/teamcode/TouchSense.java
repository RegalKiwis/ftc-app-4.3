package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.TouchSensor;
import com.qualcomm.robotcore.util.RobotLog;

import org.firstinspires.ftc.robotcore.external.Telemetry;

/**
 * Wrapper class for the touch sensor, which can detect whether its button is depressed.  It
 * is currently designed for a binary touch sensor that outputs either 0 or 1 rather than
 * a gradient of possible values.
 */
public class TouchSense implements Hardware {
    private com.qualcomm.robotcore.hardware.TouchSensor touchSensor;
    private Telemetry telemetry;

    @Override
    public void initHardware(HardwareMap hwMap, Telemetry telemetry) {
        try {
            this.touchSensor = hwMap.touchSensor.get("TouchSense");
            this.telemetry = telemetry;
        } catch (NullPointerException nullTouch) {
            RobotLog.aa("TouchSense", "Hardware initialization failed");
            throw new NullPointerException();
        }
    }

    //Readings
    public boolean isPressed() {
        return this.touchSensor.isPressed();
    }

    @Override
    public void telemetryData() {
        telemetry.addData("Touch Sensor Pressed? ", isPressed());
    }
}
