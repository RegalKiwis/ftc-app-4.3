package org.firstinspires.ftc.teamcode.OpModes;

public enum Colors {
    WHITE, RED, BLUE, GOLD, NONE;

    @Override
    public String toString() {
        switch (this) {
            case WHITE:
                return "WHITE";
            case RED:
                return "RED";
            case BLUE:
                return "BLUE";
            case GOLD:
                return "GOLD";
            case NONE:
                return "NONE";
            default:
                return NONE.toString();
        }
    }
}
