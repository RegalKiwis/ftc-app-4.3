package org.firstinspires.ftc.teamcode;


public class AndyMarkNeverRest3_7 implements MotorInformation {
    public double encoderCount() {
        return 103;
    }

    public double RPM() {
        return 1784;
    }

    public double RPS() {
        return RPM() / 60;
    }
}
