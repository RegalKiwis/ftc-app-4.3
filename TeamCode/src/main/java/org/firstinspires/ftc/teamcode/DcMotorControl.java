package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;

/**
 * Utility class for controlling sets of multiple motors
 */
public class DcMotorControl {

    /**
     * Sets the runmode for all of the input motors
     *
     * @param runMode The runmode that the motors will be changed to run use
     * @param motors  The motors to change the runMode of
     */
    public static void setMode(DcMotor.RunMode runMode, DcMotor... motors) {
        for (int count = 0; count < motors.length; count++) {
            motors[count].setMode(runMode);
        }
    }

    /**
     * Sets the power level for all of the input motors
     *
     * @param power  The power level that the motors will be set to rotate at
     * @param motors The motors to change the power levels of
     */
    public static void setPower(double power, DcMotor... motors) {
        for (int count = 0; count < motors.length; count++) {
            motors[count].setPower(Range.clip(power, -1, 1));
        }
    }

    /**
     * Sets the motor Directions for all of the input motors
     *
     * @param direction The new direction the motors should rotate in
     * @param motors    The motors to change the rotation Directions of
     */
    public static void setDirection(DcMotor.Direction direction, DcMotor... motors) {
        for (int count = 0; count < motors.length; count++) {
            motors[count].setDirection(direction);
        }
    }

    /**
     * Sets the behavior of the motors when their powers are set to zero
     *
     * @param zeroBehavior The behavior that the motors should use when they are at zero power (Powered brake or free rotating)
     * @param motors       The motors to change the zero power behavior of
     */
    public static void setZeroBehave(DcMotor.ZeroPowerBehavior zeroBehavior, DcMotor... motors) {
        for (int count = 0; count < motors.length; count++) {
            motors[count].setZeroPowerBehavior(zeroBehavior);
        }
    }


    //Methods for checking if all of the input motors are at the given mode

    /**
     * Tells whether all of the motors in the set are set to the given zero power behavior
     *
     * @param zeroBehave The zero power behavior to check the settings of
     * @param motors     The motors whose behavior will be checked
     * @return True if ALL motors are at the zeropower behavior
     */
    public static boolean zeroBehavior(DcMotor.ZeroPowerBehavior zeroBehave, DcMotor... motors) {
        for (int count = 0; count < motors.length; count++) {
            if (motors[count].getZeroPowerBehavior() != zeroBehave) {
                return false;
            }
        }
        return true;
    }

    public static boolean motorDirection(DcMotor.Direction motorDirection, DcMotor... motors) {
        for (int count = 0; count < motors.length; count++) {
            if (motors[count].getDirection() != motorDirection) {
                return false;
            }
        }
        return true;
    }

    public static boolean runMode(DcMotor.RunMode motorMode, DcMotor... motors) {
        for (int count = 0; count < motors.length; count++) {
            if (motors[count].getMode() != motorMode) {
                return false;
            }
        }
        return true;
    }

    //Methods for interacting with motor encoders
    public static void setTargetPositions(int[] targetPositions, DcMotor... motors) {
        for (int count = 0; count < motors.length; count++) {
            motors[count].setTargetPosition(targetPositions[count]);
        }
    }

    /**
     * This method changes the target positions of the given motors by the amount
     * input.  This is done relative to the current encoder position of the motors.
     * @param encoderMovement
     * @param motors
     */
    public static void setTargetPositionRelative(int encoderMovement, DcMotor... motors) {
        int[] targetEncoderCounts = new int[motors.length];
        int[] currentEncoderCounts = DcMotorControl.currentEncoderCounts(motors);
        //Calculates the new target encoder counts
        for (int i = 0; i < motors.length; i++) {
            targetEncoderCounts[i] = currentEncoderCounts[i] + encoderMovement;
        }
        DcMotorControl.setTargetPositions(targetEncoderCounts, motors);
    }

    /**
     * Tells if ANY of the input motors have reached their target position
     *
     * @param motors The motors whose target positions are being checked
     * @return True if ANY of the motors are at their target position
     */
    public static boolean anyAtTargetPosition(DcMotor... motors) {
        for (int count = 0; count < motors.length; count++) {
            if (motors[count].getCurrentPosition() == motors[count].getTargetPosition()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Tells if ALL of the input motors have reached their target positions
     *
     * @param motors
     * @return True if ALL the motors are at their target positions
     */
    public static boolean allAtTargetPosition(DcMotor... motors) {
        for (int i = 0; i < motors.length; i++) {
            if (motors[i].getCurrentPosition() != motors[i].getTargetPosition()) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param motors
     * @return True when all given motors have zero as their current encoder count
     */
    public static boolean encodersReset(DcMotor... motors) {
        for (int count = 0; count < motors.length; count++) {
            if (motors[count].getCurrentPosition() != 0) {
                return false;
            }
        }
        return true;
    }

    public static void resetEncoders(DcMotor...motors) {
        for (int i = 0; i < motors.length; i++) {
            motors[i].setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        }
    }

    /**
     * Tells whether all of the input motors are busy.
     *
     * @param motors Motors to be tested
     * @return True if all of the motors are busy (If any one motor is not busy, then false is returned).
     */
    public static boolean allMotorBusy(DcMotor... motors) {
        for (int count = 0; count < motors.length; count++) {
            if (!motors[count].isBusy()) {
                return false;
            }
        }
        return true;
    }

    public static boolean anyMotorBusy(DcMotor...motors) {
        for (int count = 0; count < motors.length; count++) {
            if (motors[count].isBusy()) {
                return true;
            }
        }
        return false;
    }

    /**
     * When at least one of the passed in motors is not busy, then the motors will
     * be stopped, and their modes will be set to the input motor mode.
     * @param mode The mode the motors should use after at least one is no longer busy
     * @param motors The motors to operate on
     */
    public static void stopAtTarget(DcMotor.RunMode mode, DcMotor...motors) {
        if (!allMotorBusy(motors)) {
            for (DcMotor x : motors) {
                x.setMode(mode);
                x.setPower(0);
            }
        }
    }

    /**
     *
     * @param motors
     * @return
     */
    public static int[] currentEncoderCounts(DcMotor... motors) {
        int[] encoderValues = new int[motors.length];
        for (int i = 0; i < motors.length; i++) {
            encoderValues[i] = motors[i].getCurrentPosition();
        }
        return encoderValues;
    }

    /**
     * Calculates the encoder position required to move a motor back to its original position
     * from the start of a match using the shortest distance possible.
     * @param motor The motor that is to be reset
     * @param encoderCountPerRotation The number of encoder counts per rotation for the motor being reset
     * @see MotorInformation
     * @return The encoder count that the motor needs to move to be in its original position
     */
    public static int repositionTarget(DcMotor motor, double encoderCountPerRotation) {
        //Divides the current encoder count by the number needed per revolution and assigns the remainder
        double repositionMovement = motor.getCurrentPosition() % encoderCountPerRotation;

        repositionMovement = repositionMovement >= (encoderCountPerRotation / 2) ?
                encoderCountPerRotation - repositionMovement : repositionMovement * -1;

        double repositionTarget = motor.getCurrentPosition() + repositionMovement;
        return (int) repositionTarget;
    }

    /**
     * Simple algorithm for calculating the power value a motor should be moving at based on the
     * distance left to reach a target stopping point and the distance from that target when deceleration should begin.
     * @param decelerationDistance The value at which the power value that is returned should begin decreasing proportionally to the remaining distance
     * @param power The initial power value before deceleration takes place
     * @param currentDistance The distance remaining to reach the target
     * @return The input value adjusted proportionally to the remaining distance to a target.
     */
    public static double proportionalBreakingPower(double decelerationDistance, double power, double currentDistance, double minSpeed) {
        double proportionalPower = Math.abs(currentDistance) <= decelerationDistance ? Math.abs(currentDistance) / decelerationDistance * power : power;
        return RangeControl.rangeCorrectMin(proportionalPower, minSpeed);
    }

    /**
     * Roughly calculates the instantaneous speed of a given motor in encoder clicks per second
     * by finding the difference in encoder readings after the given number of milliseconds.
     * Importantly, the method makes no distinction in the direction of the motor, so the returned
     * speed will be the same regardless of direction.
     * @param motor
     * @param millisecAccuracy
     * @return
     */
    public static double encoderCountPerSec(DcMotor motor, double millisecAccuracy) {
        ElapsedTime motorSecondCounter = new ElapsedTime(ElapsedTime.Resolution.MILLISECONDS);
        double originalCount = motor.getCurrentPosition();
        while (motorSecondCounter.milliseconds() <= millisecAccuracy) {
            //Waiting for the input milliseconds to pass
        }
        return Math.abs((motor.getCurrentPosition() - originalCount) * (1000/millisecAccuracy));
    }
}
