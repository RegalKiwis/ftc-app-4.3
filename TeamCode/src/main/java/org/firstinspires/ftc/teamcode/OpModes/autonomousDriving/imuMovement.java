package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.teamcode.DcMotorControl;
import org.firstinspires.ftc.teamcode.adafruitIMU;

@Disabled
abstract class imuMovement extends AutonomousBase {
    private final adafruitIMU imu = robot.imu(); //Reference variable to make code more succinct

    /**
     * The distance from the target location when driving using encoders in inches that the robot will begin slowing down its speed
     * proportional to its original power if proportionalBreaking is set to true.
     */
    protected final double breakingDistance = 10; //Inches

    /**
     * Method for driving the robot during autonomous.
     * It adjusts speed according to the remaining distance to a given target using the encoders.
     * Speed is also adjusted on the left and right motors to correct for directional deviation
     * using measurements from the Internal Motion Unit.
     */
    public void sensorCorrectedDriving(double inches, double encoderTarget, double power, float initialHeading) {
        double adjustment, leftSpeed, rightSpeed;
        double currentDistance = robot.driveTrain().calculateMovements.distanceRemaining(encoderTarget);
        power = DcMotorControl.proportionalBreakingPower(breakingDistance, power, currentDistance, robot.driveTrain().getMinSpeed());

        adjustment = proportionalTurningPower(error(initialHeading), power, .04);
        if (inches < 0) {
            adjustment *= -1;

            leftSpeed = power + adjustment;
            rightSpeed = power - adjustment;

            if (rightSpeed > 1) {
                leftSpeed = leftSpeed + (1 - rightSpeed);
                rightSpeed = 1;
            }

        } else {
            leftSpeed = power + adjustment;
            rightSpeed = power - adjustment;

            if (leftSpeed > 1) {
                rightSpeed = rightSpeed + (1 - leftSpeed);
                leftSpeed = 1;
            }
        }
        telemetry.addData("Adjustment", adjustment);

        robot.driveTrain().setCurrentLeftSpeed(leftSpeed);
        robot.driveTrain().setCurrentRightSpeed(rightSpeed);
    }

    /**
     * +Angle is CCW (LEFT), and -angle is CW (RIGHT)
     * @param angleTurn Number of degrees the robot should rotate
     * @param speed The power of the drivetrain motors while turning
     * @param timeOutS The number of seconds that a turn will be attempted for before timing out
     */
    public void imuTurn(float angleTurn, double speed, int timeOutS) {
        float targetAngle = imu.currentOrientation() + angleTurn;

        telemetry.clearAll();

        double leftPower;
        double rightPower;

        double targetTime = timeOutTarget(timeOutS);
        while (opModeIsActive() && !onHeading(targetAngle) && timeRemaining(targetTime)) {
            leftPower = proportionalTurningPower(error(targetAngle), speed, .02); //Negative error will rotate the left motor backwards, rotating the robot CCW
            rightPower = -leftPower;

            robot.driveTrain().setCurrentLeftSpeed(leftPower);
            robot.driveTrain().setCurrentRightSpeed(rightPower);

            turningTelemetry(angleTurn, targetAngle);
            telemetry.update();
        }
        robot.stopRobot();
    }



    protected void imuTurn(float angleTurn, int timeOutS) {
        this.imuTurn(angleTurn, robot.driveTrain().getSlowerTurnSpeed(), timeOutS);
    }

    protected void imuTurn(float angleTurn, double speed) {
        this.imuTurn(angleTurn, speed, getDefaultTimeOutS());
    }

    /**
     * +Angle is CCW (LEFT), and -angle is CW (RIGHT)
     * @param angleTurn
     */
    protected void imuTurn(float angleTurn) {
        this.imuTurn(angleTurn, robot.driveTrain().getSlowerTurnSpeed(), getDefaultTimeOutS());
    }

    private float error(float targetAngle) {
        return AngleUnit.DEGREES.normalize(targetAngle - imu.currentOrientation()); //A negative error means that the robot should rotate to the left which occurs with a positive relative turning amount
    }

    private boolean onHeading(float targetAngle) {
        return Math.abs(error(targetAngle)) < imu.turningTolerance();
    }

    /**
     * Calculates the power of the robot as it moves closer to a target angle while turning.  It decelerates as it moves closer.
     * @param GAIN_COEFF Proportional gain coefficient controlling how much power the robot's drive train loses as it moves closer to its target.
     *        Lower numbers will result in a faster transition during turns and will lower the speed more quickly.
     * @return
     */
    double proportionalTurningPower(float error, double power, double GAIN_COEFF) {
        double turningPower = Range.clip(error * GAIN_COEFF, -1, 1);

        return turningPower * power * -1;
    }

    /**
     * Drives the robot a given distance using the internal motion unit's linear acceleration to
     * track the robot's position.  Always uses proportional breaking and heading correction.
     * @param timeoutS
     * @param speed
     * @param distanceInch
     */
    private void imuDrive(int timeoutS, double speed, double distanceInch) {
        //Reset positional information
        robot.imu().beginReadings();
        //Initial robot heading
        float initHeading = robot.imu().currentOrientation();
        //Calculate target distance reading from IMU
        double encoderTarget = robot.driveTrain().calculateMovements.getEncoderTarget(distanceInch);

        //Get timeout time
        double targetTime = timeOutTarget(timeoutS);
        double startTime = targetTime - timeoutS;
        double currentDistance = robot.imu().linearAcceleration() * (getRuntime() - startTime);
        //Begin robot movement
        robot.driveTrain().setCurrentSpeed(speed);
        while (opModeIsActive() && currentDistance >= distanceInch && timeRemaining(targetTime)) {
            sensorCorrectedDriving(distanceInch, encoderTarget, speed, initHeading);
            telemetry.addData("Target Distance", distanceInch);
            telemetry.addData("Current Position", currentDistance);
            //Update telemetry
            telemetry.update();
            idle();
            currentDistance = robot.imu().linearAcceleration() * (getRuntime() - startTime);
        }
        robot.stopRobot();
    }

    public void imuDrive(double distanceInch) {
        this.imuDrive(getDefaultTimeOutS(), robot.driveTrain().getAutonomousDriveSpeed(), distanceInch);
    }

    private void turningTelemetry(double angleTurn, float targetAngle) {
        telemetry.addData("Power", robot.driveTrain().getLeftMotors()[0].getPower());
        telemetry.addData("Currently turning: ", String.valueOf(angleTurn) + "degrees");
        telemetry.addData("Target Angle: ", targetAngle);
        telemetry.addData("Error", error(targetAngle));
        telemetry.addData("On heading", onHeading(targetAngle));
    }
}
