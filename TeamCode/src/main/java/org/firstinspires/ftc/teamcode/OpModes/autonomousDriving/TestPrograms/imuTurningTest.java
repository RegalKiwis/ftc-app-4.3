package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.TestPrograms;

import org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.*;

@com.qualcomm.robotcore.eventloop.opmode.Autonomous(name = "IMU Turning Test", group = "Testing")
public class imuTurningTest extends segmentPaths {
    @Override
    public void runOpMode() throws InterruptedException {
        onPlay();

        imuTurn(90); //Turn CCW 90 degrees
    }
}
