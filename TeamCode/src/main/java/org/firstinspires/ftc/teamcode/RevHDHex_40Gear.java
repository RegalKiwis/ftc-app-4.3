package org.firstinspires.ftc.teamcode;

public class RevHDHex_40Gear implements MotorInformation {
    @Override
    public double encoderCount() {
        return 2240 / 2.0;
    }

    @Override
    public double RPM() {
        return 150;
    }

    @Override
    public double RPS() {
        return RPM() / 60;
    }
}
