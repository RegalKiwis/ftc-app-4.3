package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.TestPrograms.sensorReadings;

import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.OpModes.DriverControlledBase;

/**
 * OpMode that reports back telemetry data detecting image targets using Vuforia.
 */
@TeleOp(name = "Vuforia Readings", group = "Sensor")
public class VuforiaData extends DriverControlledBase {
    @Override
    public void loop() {
        robot.vuforia().telemetryData();
    }
}