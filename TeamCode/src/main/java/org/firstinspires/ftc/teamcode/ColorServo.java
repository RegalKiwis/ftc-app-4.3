package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.teamcode.OpModes.Colors;

/**
 * Wrapper class for using a markerServo arm through the shared Hardware interface.
 */
public class ColorServo implements Hardware {
    private Servo servo;
    private Telemetry telemetry;
    private DistColorSensor DistColorSensor = new DistColorSensor();

    @Override
    public void initHardware(HardwareMap hwMap, Telemetry telemetry) {
        servo = hwMap.get(Servo.class, "colorServo");
        DistColorSensor.initHardware(hwMap, telemetry);

        this.telemetry = telemetry;
    }

    @Override
    public void telemetryData() {
       telemetry.addData("Current markerServo position", servo.getPosition());
       telemetry.addData("Gold Detected", goldDetect());
       telemetry.addData("White Detected", whiteDetect());
       DistColorSensor.telemetryData();
       telemetry.update();
    }

    public void extend() {
        servo.setPosition(135.0/270);
    }

    public void retract() {
        servo.setPosition(260.0/270);
    }

    public boolean goldDetect() {
        return DistColorSensor.detectColor(Colors.GOLD) && DistColorSensor.objectDetected();
    }

    public boolean whiteDetect() {
        return DistColorSensor.objectDetected() && DistColorSensor.detectColor(Colors.WHITE);
    }
}
