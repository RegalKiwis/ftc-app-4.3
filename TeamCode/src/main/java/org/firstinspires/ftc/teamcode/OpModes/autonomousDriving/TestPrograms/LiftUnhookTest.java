package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.TestPrograms;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.segmentPaths;

/**
 * Run this program to test the method used to lower the robot at the start of autonomous
 * and then unlatch from the hook.
 */
@Autonomous(name = "Lift Unhook Test", group = "TEST")
public class LiftUnhookTest extends segmentPaths {
    @Override
    public void runOpMode() {
        //Autonomous opMode settings go in "onPlay"
        onPlay();

        //Put code to run here
        lowerAndUnhookRobot();
    }
}