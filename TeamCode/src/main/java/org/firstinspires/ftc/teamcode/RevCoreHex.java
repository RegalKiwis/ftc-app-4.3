package org.firstinspires.ftc.teamcode;

public class RevCoreHex implements MotorInformation {
    @Override
    public double encoderCount() {
        return 288;
    }

    @Override
    public double RPM() {
        return 125;
    }

    @Override
    public double RPS() {
        return RPM() / 60;
    }
}
