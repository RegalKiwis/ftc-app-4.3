package org.firstinspires.ftc.teamcode;

public enum Directions {
    FORWARD, BACKWARD, LEFT, RIGHT;

    public Directions opposite;

    static {
        FORWARD.opposite = BACKWARD;
        BACKWARD.opposite = FORWARD;
        LEFT.opposite = RIGHT;
        RIGHT.opposite = LEFT;
    }

    public static Directions currentDiretion(double distanceToMove) {
        if (distanceToMove > 0) { //Returns the direction FORWARD for positive distances
            return FORWARD;
        } else {
            return BACKWARD;
        }
    }
}
