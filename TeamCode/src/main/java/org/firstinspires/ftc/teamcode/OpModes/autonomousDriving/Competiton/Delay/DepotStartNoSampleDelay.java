package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.Competiton.Delay;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.segmentPaths;

/**
 * Autonomous program for driving through the depot to the crater without sampling.  Either alliance.  Starts
 * opposite from the depot in the lander area.  Waits four seconds before beginning to avoid alliance conflicts.
 */
@Autonomous(name = "Depot Start No Sample Short Delay", group = "RED")
public class DepotStartNoSampleDelay extends segmentPaths {
    @Override
    public void runOpMode() {
        //Autonomous opMode settings go in "onPlay"
        onPlay(startDelayDefault);

        //Put code to run here
        driveToDepot(true, false);
        depotToCrater(true, false);
    }
}