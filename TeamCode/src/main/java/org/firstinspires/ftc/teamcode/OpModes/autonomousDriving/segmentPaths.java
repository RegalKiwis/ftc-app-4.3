package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving;

/**
 * This class is conceptually a variable, except it is a class.
 * All autonomous code (code that needs to use code from his class's superclasses) should extent this class.
 * The purpose of this class is to allow for additional superclasses to be added and only need to change the "extends" line
 * to accommodate these changes in one place, this class.  In addition, sets of movements (states) that are used in multiple autonomous modes (e.g.
 * driving from a starting point to a location on a field) can be made into methods of this class.
 * Example:
 * The class that would need to be extended by all autonomous classes without this class would be "VuforiaNav".
 * An additional class "foo" is created that inherits "VuforiaNav", and now all autonomous classes must inherit it instead.
 * Rather than changing the class that each autonomous class extends to "foo", you can instead change just this class to
 * jewel "foo".
 */

public abstract class segmentPaths extends touchDrive {
    private double firstMoveDist = 17.55;
    protected final double drivingTestingDist = 35;//Used by the encoder driving test class

    private void depositTeamMarker() {
        robot.markerServo().extend();
        actionDelay();
    }

    /**
     * Drives the robot from the lander into the depot and deposits the team marker.  The path varies based on
     * the robot's starting location and whether there are plans to hit minerals.
     * @param oppositeDepotStart Whether the robot starts directly opposite the depot
     * @param hitFirstMineral Whether the robot should hit minerals during autonomous.
     */
    protected void driveToDepot(boolean oppositeDepotStart, boolean hitFirstMineral) {
        lowerAndUnhookRobot();
        if (hitFirstMineral) {
            scoreFirstMineral(oppositeDepotStart);

            if (oppositeDepotStart) {
                encoderDrive(22);
                imuTurn(45);
                encoderDrive(-55);
                depositTeamMarker();
            } else {
                encoderDrive(-35.5);
                imuTurn(45);
                encoderDrive(48);
                depositTeamMarker();
            }
        } else {
            encoderDrive(firstMoveDist);
            imuTurn(90);
            encoderDrive(50);
            imuTurn(45);
            if (oppositeDepotStart) {
                encoderDrive(-55);
            } else {
                encoderDrive(48);
            }
            depositTeamMarker();
        }
    }

    /**
     * Moves the robot from its stopping point (after unhooking and only if the moveToPosition parameter is true)
     * to the minerals and then knocks over the gold one.
     * @param oppositeDepotStart Whether the robot starts directly opposite the depot
     */
    protected void scoreFirstMineral(boolean oppositeDepotStart, boolean moveToPosition) {
        if (moveToPosition) {
            encoderDrive(firstMoveDist);
        }
        alignEdgeMineral(oppositeDepotStart);
        hitGoldMineral(oppositeDepotStart, false);
    }

    protected void scoreFirstMineral(boolean oppositeDepotStart) {
        scoreFirstMineral(oppositeDepotStart, true);
    }



    /**
     * The robot will drive to the crater from where the robot's ending position
     * from the method "driveToDepot" that drives the robot to the depot.  The
     * exact path varies with the two parameters depending on the robot's original
     * starting location and whether or not the second mineral should be hit.  This method assumes
     * the robot's front is facing the wall on our alliance side that is also opposite the crater
     * on our alliance side and is parked in the depot.
     * @param oppositeDepotStart Whether the robot's starting position was opposite the depot
     * @param hitSecondMineral Whether the gold mineral set in the second set of minerals encountered during autonomous
     */
    protected void depotToCrater(boolean oppositeDepotStart, boolean hitSecondMineral) {
        if (hitSecondMineral) {
            if (oppositeDepotStart) {
                encoderDrive(-70);
                imuTurn(-90);
                encoderDrive(-20);
                imuTurn(45);
                hitGoldMineral(false, true);
                imuTurn(-45);//Since the robot returns to its position from before hitting the mineral after hitting the gold mineral, the following movements
                encoderDrive(20);
                imuTurn(90);
                encoderDrive(-20);
            } else {
                encoderDrive(-40);
                imuTurn(45);
                hitGoldMineral(true, true);
                imuTurn(-45);
                encoderDrive(-50);
            }
        } else {
            encoderDrive(-90);
        }
    }


    private double frontToColorSensor = 15.5;
    private double robotMidPoint = robot.getRobotLength() / 2.0;
    private double midSensorDiff = Math.abs(frontToColorSensor - robotMidPoint);

    /**
     * Assumes the robot is in front of a mineral in the center.
     * Drives the robot so that the color sensor is aligned with one of the two minerals on the edge / in the corners.
     * @param depotMinerals Whether the minerals being scored are directly across from the depot
     */
    private void alignEdgeMineral(boolean depotMinerals) {
        float angleTurn;
        if (depotMinerals) {
            angleTurn = 90;
        } else {
            angleTurn = -90;
        }

        imuTurn(angleTurn);
        actionDelay();


        encoderDrive(midSensorDiff); //Moves the robot after rotating so that the middle mineral is aligned with the color sensor
        actionDelay();
    }

    /**
     * Assumes the robot has rotated so that the color sensor, once extended, will be
     * on the side of the robot where the minerals laid in autonomous will be visible.
     * Drives the robot to each color sensor location to check for the gold mineral and then stops
     * and retracts the servo arm with the color sensors on it once the gold mineral has been detected.
     */
    protected void driveUntilGold() {
        robot.colorServo().extend();
        actionDelay();

        if (!robot.colorServo().goldDetect()) {
            encoderDrive(14.5);
            actionDelay();
            if (!robot.colorServo().whiteDetect()) { //Begins to drive back and forth if nothing detected
                driveTilObject();
            }
        }

        if (!robot.colorServo().goldDetect()) {
            encoderDrive(-1 * 14.5 * 2);
            actionDelay();
            if (!robot.colorServo().whiteDetect()) { //Begins to drive back and forth if nothing detected
                driveTilObject();
            }
        }

        robot.colorServo().retract();
    }

    /**
     * Drives the robot back and forth starting with a small distance and increasing over time until
     * the Color Range Sensor detects a gold or white mineral.
     * This is so that if the robot stops slightly too far or too short from a mineral,
     * it is able to realign with it.  The robot will give up once the distance that needs to be moved
     * exceeds 15 inches, which is slightly greater than the 14.5  inch distance between the minerals;
     * this is meant as a safety feature to prevent the robot from driving and colliding if the minerals
     * are impossible to detect for some reason.
     */
    private void driveTilObject() {
        double dist = 1;
        while (opModeIsActive() && !isStopRequested() && dist <= 15 && !(robot.colorServo().whiteDetect() || robot.colorServo().goldDetect())) {
            encoderDrive(dist);
            if (dist < 0) {
                dist += 1;
            }
            dist *= -1;
        }
    }

    /**
    Assumes the robot is aligned with the mineral at the back edge so that driving
    in a straight line will run the color sensor past all three minerals.
     Drives the robot past minerals until the color sensor located on the side
     detects a gold mineral and then stops the robot and rotates it to
     knock the gold mineral out of place.
     */
    private void hitGoldMineral(boolean depotMinerals, boolean secondMineralSet) {
        int initEncoderPos = robot.driveTrain().getMotors()[0].getCurrentPosition();
        float angleHitMineral = -90;
        double centerBot = -robotMidPoint;

        if (depotMinerals) {
            angleHitMineral *= -1;
            centerBot *= -1;
        }

        driveUntilGold();
        encoderDrive(centerBot); //Makes the robot centered towards mineral

        imuTurn(angleHitMineral); //Rotates robot to hit gold mineral
        int dist = 25;
        encoderDrive(-dist);//Hits mineral
        actionDelay();

        encoderDrive(dist);
        actionDelay();
        imuTurn(-angleHitMineral); //Undo previous rotation

        int finalPos = robot.driveTrain().getMotors()[0].getCurrentPosition();
        int diffPos = finalPos - initEncoderPos;

        double inches = robot.driveTrain().calculateMovements.encoderCountToInches(diffPos);

        //This split is required because the robot starts from the edge and should return directly to that position
        if (secondMineralSet) {
            if (depotMinerals) {
                inches *= -1;
            }
        } else if (!depotMinerals) {
            inches += 28;
        }
        encoderDrive(inches);//Moves the robot back to the original mineral on the edge
    }

    protected void lowerAndUnhookRobot() {
        float hookRotate = 20;
        robot.linearSlideMotor().raise();//Lower the robot to the ground
        imuTurn(hookRotate);//Rotate robot off hook
        encoderDrive(1.25 * Math.cos(20 * (Math.PI/180.0)) + .2);//Drives robot distance until hook is past latch
        robot.linearSlideMotor().lower();
        imuTurn(-hookRotate); //Undoes original rotation
    }
}
