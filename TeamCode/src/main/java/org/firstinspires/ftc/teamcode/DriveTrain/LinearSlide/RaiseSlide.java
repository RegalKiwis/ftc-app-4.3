package org.firstinspires.ftc.teamcode.DriveTrain.LinearSlide;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.segmentPaths;

/**
 * This raises the linear slide by the distance needed to reach the hook.
 */
@Autonomous(name = "Raise Slide", group = "SLIDE")
public class RaiseSlide extends segmentPaths {
    @Override
    public void runOpMode() {
        //Autonomous opMode settings go in "onPlay"
        onPlay();

        //Put code to run here
        robot.linearSlideMotor().raise();
    }
}