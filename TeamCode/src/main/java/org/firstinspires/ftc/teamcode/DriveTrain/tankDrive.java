package org.firstinspires.ftc.teamcode.DriveTrain;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.teamcode.DcMotorControl;

/**
 * Contains methods implementing the drive train abstract class for a tank style drive.
 * A tank style drive is one that uses two or four drive motors total, with one or two motors on the left
 * and right sides of the robot respectively.
 * It is assumed that all motors are mounted upright and that running the left motors forward and the right motors backward will move the robot forward
 * in  the current implementation.
 * @see DriveTrain
 */
public class tankDrive extends DriveTrain {

    //Constructor
    public tankDrive(Telemetry telemetry, double maxSpeed, DcMotor... motors) {
        setMotors(motors);
        numberOfMotors = getMotors().length;

        setMaximumDriveSpeed(maxSpeed);
        this.setTelemetry(telemetry);

        debugTesting();
    }

    @Override
    public void forwardDrive() {
        super.forwardDrive();

        DcMotorControl.setDirection(DcMotorSimple.Direction.FORWARD, getLeftMotors());
        DcMotorControl.setDirection(DcMotorSimple.Direction.REVERSE, getRightMotors());
    }


    @Override
    public void backwardDrive() {
        super.backwardDrive();

        DcMotorControl.setDirection(DcMotorSimple.Direction.REVERSE, getLeftMotors());
        DcMotorControl.setDirection(DcMotorSimple.Direction.FORWARD, getRightMotors());
    }


    @Override
    public void rotateCCW() {
        super.rotateCCW();

        DcMotorControl.setDirection(DcMotorSimple.Direction.REVERSE, getMotors());
    }


    @Override
    public void rotateCW() {
        super.rotateCW();

        DcMotorControl.setDirection(DcMotorSimple.Direction.FORWARD, getMotors());
    }
}
