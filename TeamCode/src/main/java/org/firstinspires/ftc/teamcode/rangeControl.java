package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.util.Range;

public class RangeControl {
    /**
     * Clips a number to be less than or
     * equal to the maximum value or its inverse, whichever is closer.
     * @param num The number being clipped
     * @param maxNum The maximum value allowed for num
     * @return The clipped number
     */
    public static double rangeCorrectMax(double num, double maxNum) {
        //Clips the value to less than the maximum value or inverse
        return Range.clip(num, -maxNum, maxNum);
    }

    /**
     * Clips a value so that it is not less than the value of 'minNum' or its inverse,
     * whichever value is closer to the original number being clipped.
     * @param num The number to clip
     * @param minNum the minimum value for that number
     * @return the clipped number
     */
    public static double rangeCorrectMin(double num, double minNum) {
        if (Math.abs(num) < minNum) {
            num = num < 0 ? -minNum : minNum;
        }
        return num;
    }

    /**
     * Clips a value to be within >= the minimum value and <= the maximum value
     * or the inverse of this range (whichever is closer to
     * the original number)
     * @param num the value to be clipped
     * @param minNum the lowest allowable value
     * @param maxNum the greatest allowable value
     * @return the clipped value
     */
    public static double rangeInBounds(double num, double minNum, double maxNum) {
        return rangeCorrectMax(rangeCorrectMin(num, minNum), maxNum);
    }
}
