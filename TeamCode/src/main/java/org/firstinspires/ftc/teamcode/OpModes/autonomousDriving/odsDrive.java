package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;

import org.firstinspires.ftc.teamcode.OpModes.Colors;

/**
 * Contains methods for driving that utilize the optical distance sensor
 */
@Disabled
abstract class odsDrive extends encoderDriving {
    /**
     * Drives the robot forward until the optical distance sensor detects
     * the color of tape specified.
     * @param tapeColor The color of tape that needs to be detected
     * @param power The raw motor speed at which the robot should move at
     */
    protected void driveToTape(Colors tapeColor, double power, boolean sensorCorrect, double inches) {
        double encoderTarget = robot.driveTrain().calculateMovements.getEncoderTarget(inches);
        float initHeading = 0;
        if (sensorCorrect) {
            initHeading = robot.imu().currentOrientation();
        }
        double targetTime = timeOutTarget(getDefaultTimeOutS());
        robot.driveTrain().setCurrentSpeed(power);
        while (opModeIsActive() && timeRemaining(targetTime) && !robot.ods().detectColor(tapeColor)) {
            robot.ods().telemetryData();
            if (sensorCorrect) {
                sensorCorrectedDriving(inches, power, encoderTarget,initHeading);
            }
        }
    }

    protected void driveToTape(Colors tapeColor, double inches, boolean sensorCorrect) {
        this.driveToTape(tapeColor, robot.driveTrain().getAutonomousDriveSpeed(), sensorCorrect, inches);
    }

    protected void driveToTape(Colors tapeColor) {
        this.driveToTape(tapeColor, 0, false);
    }

    private void tapeAlignmentCorrection(double power) {
        double proportionalPower = robot.ods().proportionalPower(power);

        robot.driveTrain().setCurrentLeftSpeed(proportionalPower);
        robot.driveTrain().setCurrentRightSpeed(-proportionalPower);
    }

    public void alignWithTape(int timeoutS, double speed, targetReachedTest test) {
        robot.ods().telemetryData();
        //Get timeout time
        double targetTime = timeOutTarget(timeoutS);
        robot.driveTrain().forwardDrive();
        //Begin robot movement
        robot.driveTrain().setCurrentSpeed(speed);
        while (opModeIsActive() && timeRemaining(targetTime) && !test.test()) {
            tapeAlignmentCorrection(speed);
            telemetry.update();
            idle();
        }
        robot.stopRobot();
    }

    public void alignWithTape(int timeoutS, targetReachedTest test) {
        this.alignWithTape(timeoutS, robot.driveTrain().getAutonomousDriveSpeed(), test);
    }

    public void alignWithTape(double speed, targetReachedTest test) {
        this.alignWithTape(getDefaultTimeOutS(), speed, test);
    }

    public void alignWithTape(targetReachedTest test) {
        alignWithTape();
    }

    //The current value test for the optical distance sensor
    private targetReachedTest noTest = new targetReachedTest() {
        @Override
        public boolean test() {
            return true;
        }
    };

    public void alignWithTape() {
        this.alignWithTape(getDefaultTimeOutS(), robot.driveTrain().autonomousDriveSpeed, noTest);
    }
}
