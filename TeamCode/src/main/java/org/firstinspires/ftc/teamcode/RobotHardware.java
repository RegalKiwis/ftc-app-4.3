package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.HardwareMap;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.teamcode.DriveTrain.DriveTrain;
import org.firstinspires.ftc.teamcode.DriveTrain.tankDrive;

/**
 * Stores all of the robot's hardware along with methods that involve changes to many pieces of hardware at once and methods for accessing sets of robot hardware.
 * <p/>
 * The RobotHardware class includes objects from user created classes that add on to the basic functionality provided by the SDK of pieces of hardware.
 * <p/>
 */
public class RobotHardware {
    //Other info
    public double getRobotLength() {
        return 16.75;
    }
    public double getRotationalCircumference() {
        return getRobotLength() * Math.PI;
    }

    private Telemetry telemetry;

    //Driving speed of robot
    private final double maxSpeed = .85; //Fastest speed used during driver controlled period
    private final double autonoSpeed = maxSpeed; //Speed used by default for autonomous driving
    private final double minimumSpeed = .15;

    //Drive  train
    private DcMotor leftBackMotor;
    private DcMotor rightBackMotor;

    private DcMotor leftFrontMotor;
    private  DcMotor rightFrontMotor;

    private tankDrive twoMotor = new tankDrive(telemetry, maxSpeed, leftBackMotor, rightBackMotor);
    private tankDrive fourMotor = new tankDrive(telemetry, maxSpeed, leftBackMotor, rightBackMotor, leftFrontMotor, rightFrontMotor);

    //Reference drive train variable
    private DriveTrain driveTrain = fourMotor; //driveTrain should equal whatever form of drive train the robot is using

    //Unused hardware
    private TouchSense touchSense = new TouchSense();

    private DistColorSensor ods = new DistColorSensor();
    private DistColorSensor DistColorSensor = new DistColorSensor();
    private PickupMechanism pickupMechanism = new PickupMechanism();
    private VuforiaNavigator vuforia = new VuforiaNavigator();

    //Used hardware
    private adafruitIMU imu = new adafruitIMU();
    private LinearSlideMotor linearSlideMotor = new LinearSlideMotor();

    private MarkerServo markerServo = new MarkerServo();
    private ColorServo colorServo = new ColorServo();

    private Hardware hardware[] = {imu, driveTrain, markerServo, linearSlideMotor, colorServo, DistColorSensor};

    /**
     * Initiates all the hardware & sets telemetry on the robot by looping through a list
     * of all the hardware and running each piece of hardware's implementation of
     * the Hardware interface method initHardware().
     */
    public void initRobot(HardwareMap hwMap, Telemetry telemetry) {
        for(int i = 0; i < hardware.length; i++) {
            hardware[i].initHardware(hwMap, telemetry);
        }
        this.telemetry = telemetry;

        telemetry.setAutoClear(true);
    }

    //Getters for the hardware objects
    public DistColorSensor ods() {
        return ods;
    }

    public DistColorSensor colorSensor() {
        return DistColorSensor;
    }

    public adafruitIMU imu() {
        return imu;
    }

    public DriveTrain driveTrain() {
        return driveTrain;
    }

    public TouchSense touchSensor() {
        return touchSense;
    }

    public VuforiaNavigator vuforia() {
        return this.vuforia;
    }

    public MarkerServo markerServo() { return this.markerServo; }

    public ColorServo colorServo() { return this.colorServo; }

    public PickupMechanism pickupMechanism() {
        return this.pickupMechanism;
    }

    public LinearSlideMotor linearSlideMotor() {return this.linearSlideMotor; }


    public MarkerServo getMarkerServo() {return this.markerServo; }

    /**
     * Calibrates the robot's sensors, sets initial
     * motor modes, and gets any initial sensor readings that
     * will be stored for the match during the autonomous period.
     */
    public void calibrate() {
        //Lock servo positions in place
        colorServo().retract();
        markerServo().retract();

        //Sensors

        //Initializes imu parameters
        imu.imuCalibrate();


        //Autonomous Movement Speeds
        //Driving
        driveTrain.setAutonomousDriveSpeed(autonoSpeed);
        driveTrain.setSlowerAutonomousSpeed(autonoSpeed);
        driveTrain.setMinSpeed(minimumSpeed);
        //Turning (Currently unused)
        driveTrain.setTurnSpeed(.95);
        driveTrain().setSlowerTurnSpeed(autonoSpeed);

        driveTrain().setIsAutonomous(true);

        debugAutonomousSpeeds();
    }

    private void debugAutonomousSpeeds() {
        //Debugging
        //Test that all speed values used in autonomous have values
        if (driveTrain().getAutonomousDriveSpeed() <= 0) {
            throw new AssertionError("Autonomous driving speed is <= 0.");
        } else if (driveTrain().getSlowerAutonomousSpeed() <= 0) {
            throw new AssertionError("The slower speed for use during autonomous " +
                    "is <= 0.");
        } else if (driveTrain().getMinSpeed() < 0) {
            throw new AssertionError("The minimum driving speed is less than zero.");
        }
    }

    /**
     * Starts the logging of sensors and other data that should not begin until the match has started.
     * Currently, this includes the following:
     * <p>
     *     1) Adafruit IMU
     * </p>
     * <p>
     *     2) The logging of VuforiaData data from the robot controller's camera
     * </p>
     */
    public void beginSensorReadings() {
        // Start the logging of measured acceleration data from imu
        imu.beginReadings();
    }

    public void stopRobot() {
        this.driveTrain().setCurrentSpeed(0);
    }
}
