package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.TestPrograms;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.segmentPaths;

/**
 * Class for testing the code used to hit the first gold mineral when starting across from the crater.
 */
@Autonomous(name = "Test Score First Mineral (Crater Start)", group = "TEST")
public class firstMineralScoreTestCrater extends segmentPaths {
    @Override
    public void runOpMode() {
        //Autonomous opMode settings go in "onPlay"
        onPlay();

        //Put code to run here
        scoreFirstMineral(false, false);
    }
}