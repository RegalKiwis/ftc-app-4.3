package org.firstinspires.ftc.teamcode.OpModes.autonomousDriving.TestPrograms;

import org.firstinspires.ftc.teamcode.DriveTrain.DriveTrain;
import org.firstinspires.ftc.teamcode.OpModes.DriverControlledBase;

public class minSpeedTest extends DriverControlledBase {
    @Override
    protected void driveRobot() {
        if (!gamepad1.x) {
            robot.driveTrain().setCurrentSpeed((-gamepad1.left_stick_y + -gamepad1.right_stick_y) / 2, DriveTrain.minSpeedSetting.USE_MIN_SPEED);
        } else {
            super.driveRobot();
        }
    }
}
